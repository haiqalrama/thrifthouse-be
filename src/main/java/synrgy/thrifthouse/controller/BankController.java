package synrgy.thrifthouse.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.bank.BankDto;
import synrgy.thrifthouse.dto.bank.InsertBankDto;
import synrgy.thrifthouse.service.BankService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/bank")
public class BankController {

    @Autowired
    private BankService bankService;
    
    @GetMapping
    public ResponseEntity<ResponseDto<List<BankDto>>> getBanks() {
        List<BankDto> result = this.bankService.getBanks();
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<ResponseDto<?>> insertBank(@RequestBody InsertBankDto bankDto) {
        this.bankService.insertBank(bankDto);
        return new ResponseEntity<>(
            new ResponseDto<>(
                null,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

}
