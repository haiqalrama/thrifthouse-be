package synrgy.thrifthouse.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import synrgy.thrifthouse.dto.rajaongkir.RajaOngkirErrorDto;
import synrgy.thrifthouse.dto.rajaongkir.RajaOngkirPostCost;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/rajaongkir")
public class RajaOngkirController {

    Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();

    @Autowired
    ObjectMapper mapper;


    @GetMapping("/province")
    public ResponseEntity<String> getProvince(@RequestParam(required = false) String id) {
        String uri = "https://api.rajaongkir.com/starter/province";
        if (id!=null) {
            uri = String.format("%s?id=%s", uri, id);
        }

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String key = dotenv.get("RAJAONGKIR_KEY");
        if(key!=null) if (key.isEmpty()) key = System.getProperty("rajaongkir.key", null);
        headers.set("key", key);

        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(null,headers);

        return restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    }

    @GetMapping("/city")
    public ResponseEntity<String> getCity(@RequestParam(required = false) String id, @RequestParam(required = false) String province) {
        String uri = "https://api.rajaongkir.com/starter/city";
        if (id!=null || province!=null){
            uri = String.format("%s?", uri);
        }

        if (id!=null) {
            uri = String.format("%sid=%s", uri, id);
            if (province != null) {
                uri = String.format("%s&", uri);
            }
        }

        if(province!=null) {
            uri = String.format("%sprovince=%s", uri, province);
        }

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String key = dotenv.get("RAJAONGKIR_KEY");
        if(key!=null) if (key.isEmpty()) key = System.getProperty("rajaongkir.key", null);
        headers.set("key", key);

        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(null,headers);

        return restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    }

    @PostMapping("/cost")
    public ResponseEntity<String> getCost(@RequestBody RajaOngkirPostCost ongkirPostCost) {
        final String uri = "https://api.rajaongkir.com/starter/cost";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String key = dotenv.get("RAJAONGKIR_KEY");
        if(key!=null) if (key.isEmpty()) key = System.getProperty("rajaongkir.key", null);
        headers.set("key", key);

        MultiValueMap<String, Object> bodyPair = new LinkedMultiValueMap();
        bodyPair.add("origin", ongkirPostCost.getOrigin());
        bodyPair.add("destination", ongkirPostCost.getDestination());
        bodyPair.add("weight", ongkirPostCost.getWeight());
        bodyPair.add("courier", ongkirPostCost.getCourier());

        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(bodyPair,headers);

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<Object> badRequestHandler(final Exception ex) {
        String payload = ex.getMessage().substring(ex.getMessage().indexOf("\"") + 1, ex.getMessage().lastIndexOf("\""));
        System.out.println("test: "+payload);
        RajaOngkirErrorDto errors = null;
        try {
            errors = mapper.readValue(payload, RajaOngkirErrorDto.class);
        } catch (JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        HttpStatus status = HttpStatus.resolve(errors.getRajaongkir().getStatus().getCode());
        if (status != null) {
            return ResponseEntity.status(status).body(errors);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }
}
