package synrgy.thrifthouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.address.AddressResponseDto;
import synrgy.thrifthouse.model.Address;
import synrgy.thrifthouse.service.AddressService;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/users/")
public class AddressController {

    private AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping("/addresses")
    public ResponseEntity<ResponseDto<List<AddressResponseDto>>> getAll() {
        return ResponseEntity.ok(new ResponseDto<>(this.addressService.getAll(), 200, "Success"));
    }

    @GetMapping("/{userId}/addresses")
    public ResponseEntity<ResponseDto<List<AddressResponseDto>>> getAllByUserId(
            @PathVariable UUID userId
        ){
        return ResponseEntity.ok(new ResponseDto<>(this.addressService.getAllByUserId(userId), 200, "Success"));
    }

    @GetMapping("/{userId}/addresses/{addressId}")
    public ResponseEntity<ResponseDto<AddressResponseDto>> getByAddressId(
            @PathVariable UUID userId, @PathVariable UUID addressId
    ){
        return ResponseEntity.ok(new ResponseDto<>(this.addressService.getByAddressId(userId, addressId), 200, "Success"));
    }

    @PostMapping("/{userId}/addresses")
    public ResponseEntity<ResponseDto<AddressResponseDto>> store(
            @PathVariable UUID userId, @RequestBody AddressResponseDto addressResponseDto
            ){
        return ResponseEntity.ok(new ResponseDto<>(this.addressService.store(userId, addressResponseDto), 201, "created"));
    }

    @PutMapping("/{userId}/addresses/{addressId}")
    public ResponseEntity<ResponseDto<AddressResponseDto>> store(
            @PathVariable UUID userId, @PathVariable UUID addressId, @RequestBody AddressResponseDto addressResponseDto
    ){
        return ResponseEntity.ok(new ResponseDto<>(this.addressService.updateAddress(userId, addressId, addressResponseDto), 200, "created"));
    }

    @DeleteMapping("/{userId}/addresses/{addressId}")
    public ResponseEntity<ResponseDto<Map<String, UUID>>> deleteByAddressId(
            @PathVariable UUID userId, @PathVariable UUID addressId
    ){
        return ResponseEntity.ok(new ResponseDto<>(this.addressService.deleteByAddressId(userId, addressId), 200, "Success"));
    }

    // Di Undur
//    @PutMapping("/{userId}/addresses/{addressId}")
//    public ResponseEntity<ResponseDto<AddressResponseDto>> updateAddressLabel(
//            @PathVariable UUID userId, @PathVariable UUID addressId,  @RequestBody AddressResponseDto addressResponseDto
//    ){
//        return ResponseEntity.ok(new ResponseDto<>(this.addressService.update(userId, addressId, addressResponseDto), 200, "Success"));
//    }
}
