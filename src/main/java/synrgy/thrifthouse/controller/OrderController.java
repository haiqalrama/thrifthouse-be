package synrgy.thrifthouse.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.order.InsertOrderDtoNew;
import synrgy.thrifthouse.dto.order.OrdersPaymentDto;
import synrgy.thrifthouse.service.OrderService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/orders")
public class OrderController {
    
    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseEntity<ResponseDto<Object>> insertOrder(@RequestBody InsertOrderDtoNew insertOrderDto) {
        Object result = this.orderService.insertOrderNew(insertOrderDto);
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                201,
                "created"
            ),
            HttpStatus.OK
        );
    }

    @GetMapping("/{id}/payment")
    public ResponseEntity<ResponseDto<OrdersPaymentDto>> getOrdersPayment(@PathVariable UUID id) {
        OrdersPaymentDto result = this.orderService.getOrdersPayment(id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @PutMapping("{id}/payment/receipt")
    public ResponseEntity<ResponseDto<Map<String,String>>> insertReceipt(@RequestParam("photo") MultipartFile image, @PathVariable UUID id) {
        String s = orderService.storePaymentReceipt(image, id);
        Map<String, String> result = new HashMap<>();
        result.put("id", id.toString());
        result.put("url", s);
        return ResponseEntity.ok(new ResponseDto<>(result,200,"success"));
    }
    
}
