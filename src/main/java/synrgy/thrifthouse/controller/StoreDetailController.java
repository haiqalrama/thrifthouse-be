package synrgy.thrifthouse.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.product.StoreInformationDto;
import synrgy.thrifthouse.dto.store.ProductsOfStoreDto;
import synrgy.thrifthouse.dto.store.StoreDetailBannerDto;
import synrgy.thrifthouse.dto.store.StoreKtpDto;
import synrgy.thrifthouse.dto.storeSettings.InformationDto;
import synrgy.thrifthouse.dto.storeSettings.InformationDto2;
import synrgy.thrifthouse.dto.storeSettings.StoreAddressDto;
import synrgy.thrifthouse.dto.storeSettings.StoreBankDto;
import synrgy.thrifthouse.dto.storeSettings.StoreDeliveryServiceDto;
import synrgy.thrifthouse.dto.user.SellerAccountDto;
import synrgy.thrifthouse.dto.user.SellerAccountDto2;
import synrgy.thrifthouse.dto.store.StoreCreatePostDto;
import synrgy.thrifthouse.service.StoreService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/stores")
public class StoreDetailController {
    
    @Autowired
    private StoreService storeService;

    @GetMapping("/{id}/banner")
    public ResponseEntity<ResponseDto<StoreDetailBannerDto>> getBanner(@PathVariable UUID id) {
        StoreDetailBannerDto bannerDto = this.storeService.getBannerByStoreId(id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                bannerDto,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @GetMapping("/{id}/logo")
    public ResponseEntity<ResponseDto<Map<String, String>>> getLogo(@PathVariable UUID id) {
        Map<String, String> result = this.storeService.getLogoByStoreId(id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }
    
    @GetMapping("/{id}/informations")
    public ResponseEntity<ResponseDto<StoreInformationDto>> getStoreDetails(@PathVariable UUID id) throws JsonMappingException, JsonProcessingException, IOException {
        StoreInformationDto storeInformationDto = this.storeService.getStoreInformationById(id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                storeInformationDto,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @GetMapping("/{id}/products")
    public ResponseEntity<ResponseDto<List<ProductsOfStoreDto>>> getStoresProducts(
        @PathVariable UUID id,
        @RequestParam(defaultValue = "0") Integer page,
        @RequestParam(defaultValue = "8") Integer size,
        @RequestParam(required = false) String category
    ) {
        List<ProductsOfStoreDto> result = this.storeService.getStoreProducts(page, size, id, category);
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @GetMapping("/{id}/about")
    public ResponseEntity<ResponseDto<Object>> getStoreAbout(@PathVariable UUID id) {
        Object result = this.storeService.getStoreAbout(id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                200,
                "success"
            ),
            HttpStatus.OK
        );
        
    }

    @GetMapping("/{id}/delivery-service")
    public ResponseEntity<ResponseDto<StoreDeliveryServiceDto>> getStoreDeliveryService(@PathVariable UUID id) {
        return new ResponseEntity<>(
            new ResponseDto<>(
                this.storeService.getStoreDeliveryService(id),
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @PutMapping("/{id}/delivery-service")
    public ResponseEntity<ResponseDto<StoreDeliveryServiceDto>> updateStoreDeliveryService(@PathVariable UUID id, @RequestBody StoreDeliveryServiceDto deliveryServiceDto) {
        try {
            StoreDeliveryServiceDto response = this.storeService.updateStoreDeliveryService(id, deliveryServiceDto);
            return new ResponseEntity<>(
                new ResponseDto<>(
                    response,
                    200,
                    "success"
                ),
                HttpStatus.OK
            );
        } catch (Exception e) {
            System.out.println("StoreDetailController: " + e);
            throw e;
        }
    }

    @PostMapping(value = "/user/{userId}", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<ResponseDto<JsonNode>> createStore(
            @PathVariable UUID userId,
            StoreCreatePostDto storeCreatePostDto
    ) {
        JsonNode newStore = storeService.createStore(userId, storeCreatePostDto);
        return ResponseEntity.ok(new ResponseDto<>(newStore, 201, "created"));
    }

    @GetMapping("/{store_id}/store-information")
    public ResponseEntity<ResponseDto<InformationDto2>> getStoreInformation(@PathVariable UUID store_id) {
        InformationDto2 response = this.storeService.getStoreInformation(store_id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                response,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @PutMapping("/{store_id}/store-information")
    public ResponseEntity<ResponseDto<InformationDto2>> updateStoreInformation(
        @PathVariable UUID store_id,
        @ModelAttribute InformationDto informationDto
    ) {
        try {
            InformationDto2 response = this.storeService.updateStoreInformation(store_id, informationDto);
            return new ResponseEntity<>(
                new ResponseDto<>(
                    response,
                    200,
                    "success"
                ),
                HttpStatus.OK
            );
        } catch (Exception e) {
            System.out.println("StoreDetailController: " + e);
            throw e;
        }
    }

    @GetMapping("/{store_id}/store-bank-account")
    public ResponseEntity<ResponseDto<StoreBankDto>> getStoreBankAccount(@PathVariable UUID store_id) {
        StoreBankDto response = this.storeService.getStoreBankAccount(store_id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                response,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @PutMapping("/{store_id}/store-bank-account")
    public ResponseEntity<ResponseDto<StoreBankDto>> updateStoreBankAccount(@PathVariable UUID store_id, @RequestBody StoreBankDto storeBankDto) {
        try {
            StoreBankDto response = this.storeService.updateStoreBankAccount(store_id, storeBankDto);
            return new ResponseEntity<>(
                new ResponseDto<>(
                    response,
                    200,
                    "success"
                ),
                HttpStatus.OK
            );
        } catch (Exception e) {
            System.out.println("StoreDetailController: " + e);
            throw e;
        }
    }

    @GetMapping("/{store_id}/store-address")
    public ResponseEntity<ResponseDto<StoreAddressDto>> getStoreAddress(@PathVariable UUID store_id) {
        StoreAddressDto response = this.storeService.getStoreAddress(store_id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                response,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @PutMapping("/{store_id}/store-address")
    public ResponseEntity<ResponseDto<StoreAddressDto>> updateStoreAddress(@PathVariable UUID store_id, @RequestBody StoreAddressDto storeAddressDto) {
        try {
            StoreAddressDto response = this.storeService.updateStoreAddress(store_id, storeAddressDto);
            return new ResponseEntity<>(
                new ResponseDto<>(
                    response,
                    200,
                    "success"
                ),
                HttpStatus.OK
            );
        } catch (Exception e) {
            System.out.println("StoreDetailController: " + e);
            throw e;
        }
    }

    @GetMapping("/{store_id}/ktp")
    public ResponseEntity<ResponseDto<StoreKtpDto>> getStoreKtp(@PathVariable UUID store_id) {
        StoreKtpDto result = this.storeService.getStoreKtp(store_id);
        return ResponseEntity.ok(new ResponseDto<>(result,200,"success"));
    }

    @GetMapping("/seller-account/{user_id}")
    public ResponseEntity<ResponseDto<SellerAccountDto2>> getSellerDetail(@PathVariable UUID user_id) {
        SellerAccountDto2 response = this.storeService.getSellerDetail(user_id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                response,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @PutMapping("/seller-account/{user_id}")
    public ResponseEntity<ResponseDto<SellerAccountDto2>> updateSellerDetail(
        @PathVariable UUID user_id,
        @ModelAttribute SellerAccountDto sellerAccount
    ) {
        try {
            SellerAccountDto2 response = this.storeService.updateSellerDetail(user_id, sellerAccount);
            return new ResponseEntity<>(
                new ResponseDto<>(
                    response,
                    200,
                    "success"
                ),
                HttpStatus.OK
            );
        } catch (Exception e) {
            System.out.println("StoreDetailController: " + e);
            throw e;
        }
        
    }

}
