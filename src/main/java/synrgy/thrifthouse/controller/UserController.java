package synrgy.thrifthouse.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.user.LoginDto;
import synrgy.thrifthouse.dto.user.UserDto;
import synrgy.thrifthouse.service.UserService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto) {
        String uriString = ServletUriComponentsBuilder.fromCurrentContextPath().path("/login").toUriString();
        URI uri = URI.create(uriString);
        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/register")
    public void register(@RequestBody UserDto userDto) {
        this.userService.insertUser(userDto);
    }

    @GetMapping("/api/v1/user")
    public ResponseEntity<ResponseDto<List<UserDto>>> getUsers() {
        List<UserDto> userDtos = this.userService.getUsers();
        ResponseDto<List<UserDto>> responseDto = new ResponseDto<>(
            userDtos,
            HttpStatus.OK.value(),
            "success"
        );
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
    
    @GetMapping("/api/v1/user/{id}")
    public ResponseEntity<ResponseDto<UserDto>> getUser(@PathVariable UUID id) {
        UserDto userDto = this.userService.getUserById(id);
        ResponseDto<UserDto> responseDto = new ResponseDto<>(
            userDto,
            HttpStatus.OK.value(),
            "success"
        );
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
        
    }

    @PutMapping("/api/v1/user/{id}")
    public ResponseEntity<ResponseDto<?>> updateUser(@RequestBody UserDto userDto, @PathVariable UUID id) {
        ResponseDto<?> responseDto = null;
        try {
            this.userService.updateUser(userDto, id);
            responseDto = new ResponseDto<>(null, 200, "success");
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            responseDto = new ResponseDto<>(null, 404, e.getMessage());
            return new ResponseEntity<>(responseDto, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/api/v1/user/seller-store/{user_id}")
    public ResponseEntity<ResponseDto<Map<String, UUID>>> getStoreIdByUserId(@PathVariable UUID user_id) {
        Map<String, UUID> result = new HashMap<>();
        result.put("store_id", this.userService.findStoreByUserId(user_id));
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

}
