package synrgy.thrifthouse.controller;

import java.io.IOException;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.product.*;
import synrgy.thrifthouse.service.ProductService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/products")
public class ProductController {

  private final ProductService productService;

  @Autowired
  public ProductController(ProductService productService) {
    this.productService = productService;
  }


  @ApiOperation(value = "Get list of products in a form of pagination with several query params: sortBy, page, size")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved")
  })
  @GetMapping("")
  public ResponseEntity<ResponseDto<PaginationProductDto<List<ProductListDto>>>> getAll(
          ProductFilter productFilter,
          @RequestParam(defaultValue = "latest") @ApiParam(value = "Sort keyword for product, accept value: latest, highest, lowest") String sortBy,
          @RequestParam(defaultValue = "0") @ApiParam(value = "Page number") Integer page,
          @RequestParam(defaultValue = "2") @ApiParam(value = "Page size") Integer size
  ) {
    return ResponseEntity.ok(new ResponseDto<>(this.productService.getAll(page, size, sortBy, productFilter), 200, "Success"));
  }

    @ApiOperation(value = "Get a product by UUID with the Store's information")
    @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Successfully retrieved"),
    @ApiResponse(code = 404, message = "The product with the corresponding UUID was not found")
  })

  @GetMapping("/{id}")
  public ResponseEntity<ResponseDto<ProductDetailDto>> getProduct(@ApiParam(value = "UUID of the product") @PathVariable  UUID id) throws IOException {
    ProductDetailDto findDetail = productService.findDetail(id);
    return ResponseEntity.ok(new ResponseDto<>(findDetail, 200, "Success"));
  }

  @GetMapping("/{id}/edit")
  public ResponseEntity<ResponseDto<ProductDto>> getById(@ApiParam(value = "UUID of the product") @PathVariable  UUID id) {
    return ResponseEntity.ok(new ResponseDto<>(productService.getById(id), 200, "Success"));
  }

  @PutMapping("/{id}")
  public ResponseEntity<ResponseDto<ProductDto>> updateProduct(
          @ApiParam(value = "UUID of the product") @PathVariable  UUID id, @ModelAttribute ProductRequestDto productRequestDto, @RequestParam(value = "changePhoto" , required = false) int[] changePhoto )  {

    return ResponseEntity.ok(new ResponseDto<>(productService.updateProduct(id, productRequestDto, changePhoto), 200, "Success"));
  }

  @GetMapping("/filters")
  public ResponseEntity<ResponseDto<Map<String, Set<Object>>>> get() {
    return ResponseEntity.ok().body(new ResponseDto<>(productService.getFilters(), 200, "success"));
  }

  @PostMapping(consumes = { MediaType.MULTIPART_FORM_DATA_VALUE} )
  public ResponseEntity<ResponseDto<ProductDto>> storeProducts(
          @ModelAttribute ProductRequestDto productRequestDto, @RequestParam("sellerId") UUID sellerId
  ) {
    return ResponseEntity.ok().body(new ResponseDto<>(productService.storeProduct(productRequestDto, sellerId), 200, "success"));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<ResponseDto<ProductFavoriteDto>> deleteProducts(
          @PathVariable UUID id
  ) {
    return ResponseEntity.ok().body(new ResponseDto<>(productService.deleteProduct(id), 200, "success"));
  }

}
