package synrgy.thrifthouse.controller;

import io.swagger.annotations.ApiParam;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.service.OrderUserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/users")
public class OrderUserController {

    private final OrderUserService orderUserService;

    public OrderUserController(OrderUserService orderUserService) {
        this.orderUserService = orderUserService;
    }

    @GetMapping("/{userId}/orders")
    public ResponseEntity<ResponseDto<List<Map<String, Object>>>> getAll(
            @PathVariable UUID userId,
            @ApiParam(value = "Status keyword, accept value: bayar, dikemas, dikirim, selesai, dibatalkan") @RequestParam(required = false) String status) {
        List<Map<String, Object>> all = orderUserService.getAll(userId, status);
        return ResponseEntity.ok(new ResponseDto<>(all, 200, "success"));
    }

    @GetMapping("/{userId}/orders/{orderId}")
    public ResponseEntity<ResponseDto<Map<String, Object>>> getOrderDetailById(
            @PathVariable UUID userId,
            @PathVariable UUID orderId,
            @ApiParam(value = "Status keyword, accept value: bayar, dikemas, dikirim, selesai, dibatalkan") @RequestParam(required = false) String status) {
        Map<String, Object> order = orderUserService.getOrderDetailById(userId, orderId);
        return ResponseEntity.ok(new ResponseDto<>(order, 200, "success"));
    }

    @PutMapping("/{userId}/orders/{transaction_id}")
    public ResponseEntity<ResponseDto<Map<String, UUID>>> acceptOrder(
        @PathVariable UUID userId,
        @PathVariable UUID transaction_id
    ) {
        Map<String, UUID> responseMap = new HashMap<>();
        try {
            this.orderUserService.acceptOrder(transaction_id);
            responseMap.put("transactionId", transaction_id);
            return new ResponseEntity<>(
                new ResponseDto<>(
                    responseMap,
                    200,
                    "success"
                ),
                HttpStatus.OK
            );
        } catch (Exception e) {
            System.out.println("OrderUserController: " + e);
            throw e;
        }
    }

}
