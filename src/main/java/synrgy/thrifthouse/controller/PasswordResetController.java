package synrgy.thrifthouse.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.service.PasswordResetService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/")
public class PasswordResetController {

    private final PasswordResetService passwordResetService;

    @Autowired
    public PasswordResetController(PasswordResetService passwordResetService) {
        this.passwordResetService = passwordResetService;
    }

    @PostMapping("/password-reset")
    public ResponseEntity<ResponseDto<Object>> createPasswordReset(@RequestBody ObjectNode body) {
        String email;
        if (body.get("email")!=null) {
            email = body.get("email").asText();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseDto<>(null, 400, "there is something missing in the request body"));
        }
        passwordResetService.createPasswordReset(email);
        return ResponseEntity.ok(new ResponseDto<>("Email has been sent", 200, "Success"));
    }

    @GetMapping("/password-reset")
    public ResponseEntity<ResponseDto<Object>> resetPassword(@RequestParam("token") String token, @RequestBody ObjectNode body) {
        String password;
        if (body.get("password")!=null) {
            password = body.get("password").asText();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseDto<>(null, 400, "there is something missing in the request body"));
        }
        passwordResetService.resetPassword(token, password);
        return ResponseEntity.ok(new ResponseDto<>("Password has been reset", 200, "Success"));
    }

}
