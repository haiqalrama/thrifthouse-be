package synrgy.thrifthouse.controller;

import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.notification.NotificationDto;
import synrgy.thrifthouse.service.NotificationService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/users")
public class NotificationController {

    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/{userId}/notifications")
    public ResponseEntity<ResponseDto<List<NotificationDto>>> getAllNotification(
            @PathVariable UUID userId,
            @ApiParam(value = "Type of Notifications. Accept value: info, pesanan") @RequestParam(required = false) String type
    ) {
        List<NotificationDto> all = notificationService.getAllNotificationByUserId(userId, type);
        return ResponseEntity.ok(
                new ResponseDto<>(all, 200, "success")
        );
    }

    @PutMapping("/{userId}/notifications/{notificationId}/read")
    public ResponseEntity<ResponseDto<NotificationDto>> markAsRead(
            @PathVariable UUID userId,
            @PathVariable UUID notificationId
    ) {
        NotificationDto notificationDto = notificationService.markAsRead(userId,notificationId);
        return ResponseEntity.ok(
                new ResponseDto<>(notificationDto, 200, "success")
        );
    }

}
