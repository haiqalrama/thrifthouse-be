package synrgy.thrifthouse.controller;

import io.swagger.annotations.ApiParam;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.review.ReviewDto;
import synrgy.thrifthouse.dto.review.ReviewListDto;
import synrgy.thrifthouse.dto.review.ReviewPostDto;
import synrgy.thrifthouse.service.ReviewService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/stores")
public class ReviewController {

    private final ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("/{storeId}/reviews")
    public ResponseEntity<ResponseDto<ReviewListDto>> getAll(
            @RequestParam(defaultValue = "0") @ApiParam(value = "Page number") Integer page,
            @RequestParam(defaultValue = "8") @ApiParam(value = "Page size") Integer size,
            @RequestParam(required = false) @ApiParam(value = "Rate number") Integer rate,
            @RequestParam(required = false) @ApiParam(value = "Photo Exist") Boolean hasPhoto,
            @PathVariable UUID storeId
    ) {
        ReviewListDto reviews = reviewService.getReviews(page, size, rate, hasPhoto, storeId);
        return ResponseEntity.ok(new ResponseDto<>(reviews, 200, "success"));
    }

    @PostMapping(value = "/{storeId}/reviews", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<ResponseDto<Object>> store(
            @PathVariable UUID storeId,
            @ModelAttribute ReviewPostDto reviewPostDto,
            @RequestParam(required = false) List<MultipartFile> photos
    ) {
        ReviewDto reviewDto= reviewService.postReview(storeId, reviewPostDto, photos);
        return ResponseEntity.ok(new ResponseDto<>(
                reviewDto,
                201,
                "created"
        ));
    }
}
