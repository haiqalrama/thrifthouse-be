package synrgy.thrifthouse.controller;

import java.io.IOException;
import java.util.*;

import com.auth0.jwt.interfaces.Claim;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.product.ProductIdDto;
import synrgy.thrifthouse.dto.product.ProductWithStoreDto;
import synrgy.thrifthouse.service.CartService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class CartController {
  private final CartService cartService;
  private final ObjectMapper mapper;

  @Autowired
  public CartController(CartService cartService, ObjectMapper mapper) {
    this.cartService = cartService;
    this.mapper = mapper;
  }

  @PostMapping("/users/{id}/cart")
  public ResponseEntity<ResponseDto<Object>> addToCart(@PathVariable UUID id, @RequestBody ProductIdDto body,
                                                       @RequestHeader("authorization") String token) {
    token = token.substring(7);
    Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
    JWTVerifier verifier = JWT.require(algorithm).build();

    DecodedJWT decodedJWT = verifier.verify(token);
    Claim sub = decodedJWT.getClaim("sub");
    Map<String, Object> map = sub.asMap();
    if (!map.get("id").equals(id.toString())) {
      throw new AccessDeniedException("logged in users cannot add products to this cart");
    }

    if (body.getProductId()!=null) {
      UUID productId = body.getProductId();
      cartService.addToCart(id, productId);
      body.setProductIds(null);
      return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseDto<>(body, 201, "created"));
    } else if (body.getProductIds()!=null) {
      Set<UUID> productIds = new HashSet<>(body.getProductIds());
      cartService.bulkAddToCart(id, productIds);
      body.setProductId(null);
      return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseDto<>(body, 201, "created"));
    }else {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseDto<>(null, 400, "there is something missing in the request body"));
    }
  }

  @DeleteMapping("/users/{id}/cart")
  public ResponseEntity<ResponseDto<Object>> removeFromCart(@PathVariable UUID id, @RequestBody ProductIdDto body,
                                                            @RequestHeader("authorization") String token) {
    token = token.substring(7);
    Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
    JWTVerifier verifier = JWT.require(algorithm).build();

    DecodedJWT decodedJWT = verifier.verify(token);
    Claim sub = decodedJWT.getClaim("sub");
    Map<String, Object> map = sub.asMap();
    if (!map.get("id").equals(id.toString())) {
      throw new AccessDeniedException("logged in users cannot delete products to this cart");
    }

    if (body.getProductId()!=null) {
      UUID productId = body.getProductId();
      cartService.removeFromCart(id, productId);
      return ResponseEntity.ok(new ResponseDto<>(null, 200, "success"));
    } else if(body.getProductIds()!=null) {
      Set<UUID> productIds = new HashSet<>(body.getProductIds());
      cartService.bulkRemoveFromCart(id, productIds);
      return ResponseEntity.ok(new ResponseDto<>(null, 200, "success"));
    } else {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseDto<>(null, 400, "there is something missing in the request body"));
    }
  }

  @GetMapping("/users/{id}/cart")
  public ResponseEntity<ResponseDto<Collection<ProductWithStoreDto>>> getCart(@PathVariable UUID id,
                                                                              @RequestHeader("authorization") String token) throws IOException {
    token = token.substring(7);
    Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
    JWTVerifier verifier = JWT.require(algorithm).build();

    DecodedJWT decodedJWT = verifier.verify(token);
    Claim sub = decodedJWT.getClaim("sub");
    Map<String, Object> map = sub.asMap();
    if (!map.get("id").equals(id.toString())) {
      throw new AccessDeniedException("logged in users cannot get products from this cart");
    }

    Collection<ProductWithStoreDto> cart = cartService.getCart(id);
    return ResponseEntity.ok(new ResponseDto<>(cart, 200, "success"));
  }
}