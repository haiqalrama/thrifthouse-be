package synrgy.thrifthouse.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import synrgy.thrifthouse.dto.ResponseDto;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final ResponseDto<Object> responseDto;

  public ResourceNotFoundException(String message) {
    super(message);
    this.responseDto = new ResponseDto<>();
    this.responseDto.setStatusCode(HttpStatus.NOT_FOUND.value());
    this.responseDto.setMessage(message);
  }

  public ResourceNotFoundException(String message, Throwable cause) {
    super(message, cause);
    this.responseDto = new ResponseDto<>();
    this.responseDto.setStatusCode(HttpStatus.NOT_FOUND.value());
    this.responseDto.setMessage(message);
  }

  public ResponseDto<Object> getResponseDto() {
    return responseDto;
  }

}
