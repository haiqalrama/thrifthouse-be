package synrgy.thrifthouse.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import synrgy.thrifthouse.dto.ResponseDto;

@ResponseStatus(value = HttpStatus.NOT_IMPLEMENTED)
public class UpdateFailedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final ResponseDto<Object> responseDto;

    public UpdateFailedException(String message) {
        super(message);
        this.responseDto = new ResponseDto<>();
        this.responseDto.setStatusCode(HttpStatus.NOT_IMPLEMENTED.value());
        this.responseDto.setMessage(message);
    }
    
}
