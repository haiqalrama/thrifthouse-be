package synrgy.thrifthouse.exception;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import synrgy.thrifthouse.dto.ResponseDto;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    super.handleTypeMismatch(ex, headers, status, request);
    return ResponseEntity.status(status).body(new ResponseDto<>(null,status.value(), ex.getMessage()));
  }

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    super.handleMissingServletRequestParameter(ex, headers, status, request);
    return ResponseEntity.status(status).body(new ResponseDto<>(null,status.value(), ex.getMessage()));
  }

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    super.handleMissingServletRequestPart(ex, headers, status, request);
    return ResponseEntity.status(status).body(new ResponseDto<>(null,status.value(), ex.getMessage()));
  }

  @ExceptionHandler(ConflictException.class)
  @ResponseBody
  public ResponseEntity<ResponseDto<Object>> resolveException(ConflictException exception) {
    ResponseDto<Object> responseDto = new ResponseDto<>();
    responseDto.setStatusCode(HttpStatus.CONFLICT.value());
    responseDto.setMessage(exception.getMessage());

    return new ResponseEntity<>(responseDto, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(RuntimeException.class)
  @ResponseBody
  public ResponseEntity<ResponseDto<Object>> resolveException(RuntimeException exception) {
    ResponseDto<Object> responseDto = new ResponseDto<>();
    responseDto.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
    responseDto.setMessage(exception.getMessage());

    return new ResponseEntity<>(responseDto, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(AccessDeniedException.class)
  @ResponseBody
  public ResponseEntity<ResponseDto<Object>> resolveException(AccessDeniedException exception) {
    ResponseDto<Object> responseDto = new ResponseDto<>();
    responseDto.setStatusCode(HttpStatus.FORBIDDEN.value());
    responseDto.setMessage(exception.getMessage());

    return new ResponseEntity<>(responseDto, HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler(BadRequestException.class)
  @ResponseBody
  public ResponseEntity<ResponseDto<Object>> resolveException(BadRequestException exception) {
    ResponseDto<Object> responseDto = exception.getResponseDto();

    return new ResponseEntity<>(responseDto, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseBody
  public ResponseEntity<ResponseDto<Object>> resolveException(ResourceNotFoundException exception) {
    ResponseDto<Object> responseDto = new ResponseDto<>();
    responseDto.setStatusCode(HttpStatus.NOT_FOUND.value());
    responseDto.setMessage(exception.getMessage());

    return new ResponseEntity<>(responseDto, HttpStatus.NOT_FOUND);
  }

}
