package synrgy.thrifthouse.dto.store;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreCreatePostDto {
    private String name;
    private String province;
    private String city;
    private String fullAddress;
    private String ktpNumber;
    private MultipartFile ktpImg;
    private String deliveryService;
    private String bankHolder;
    private String bankNumber;
    private String bankName;
}
