package synrgy.thrifthouse.dto.store;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductsOfStoreDto {
    
    private UUID storeId;
    private UUID productId;
    private String name;
    private String brand;
    private String size;
    private String condition;
    private Integer price;
    private String photos;
    private String category;

}
