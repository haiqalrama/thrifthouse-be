package synrgy.thrifthouse.dto.store;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaginationStoreDto<T> {
    private Long totalItems;
    private T stores;
    private Integer totalPages;
    private Integer currentPage;
}
