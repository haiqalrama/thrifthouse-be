package synrgy.thrifthouse.dto.store;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreSimpleDto {

  private UUID id;

  private String name;

  private String province;
  
  private String province_id;

  private String city;
  
  private String city_id;

  private String photo;
}
