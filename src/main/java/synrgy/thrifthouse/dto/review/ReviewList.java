package synrgy.thrifthouse.dto.review;

public interface ReviewList {
    Double getTotalRate();
    Long getTotalSelling();
    Long getTotalReview();
    Long getTotalRated();
}
