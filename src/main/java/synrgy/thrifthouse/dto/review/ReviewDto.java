package synrgy.thrifthouse.dto.review;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewDto {
    private String description;
    private Integer rating;
    private List<String> photos;
    private String createdAt;
    private UserSimpleDto user;

    public ReviewDto(UUID id, String description, Integer rating, String photos, String createdAt, UserSimpleDto user) {
        this.description = description;
        this.rating = rating;
        this.photos = new ArrayList<String>(
                java.util.Arrays.asList(
                        photos.substring(1, photos.length() - 1).split(", ")));;
        this.createdAt = createdAt;
        this.user = user;
    }
}
