package synrgy.thrifthouse.dto.review;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ReviewListDto {

    private Long totalItems;
    private Integer totalPages;
    private Integer currentPage;

    private Double totalRate;
    private Long totalSelling;
    private Long totalReview;
    private Long totalRated;



    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ReviewDto> review = new ArrayList<>();

    public ReviewListDto(Double totalRate, Long totalSelling, Long totalReview, Long totalRated) {
        this.totalRate = totalRate;
        this.totalSelling = totalSelling;
        this.totalReview = totalReview;
        this.totalRated = totalRated;
    }
    public ReviewListDto(Double totalRate, Long totalSelling, Long totalReview, Long totalRated,ReviewDto reviewDto) {
        this.totalRate = totalRate;
        this.totalSelling = totalSelling;
        this.totalReview = totalReview;
        this.totalRated = totalRated;
        this.review.add(reviewDto);
    }
}
