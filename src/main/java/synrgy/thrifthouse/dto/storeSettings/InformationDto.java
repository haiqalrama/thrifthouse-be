package synrgy.thrifthouse.dto.storeSettings;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InformationDto {
    
    String storeName;
    MultipartFile storeBanner;
    MultipartFile storeLogo;
    String storeStatus;
    String storeAbout;

}
