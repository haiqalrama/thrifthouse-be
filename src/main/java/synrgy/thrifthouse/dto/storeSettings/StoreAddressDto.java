package synrgy.thrifthouse.dto.storeSettings;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreAddressDto {
    
    private String fullAddress;
    private String province;
    private String city;

}
