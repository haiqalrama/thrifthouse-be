package synrgy.thrifthouse.dto.storeSettings;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InformationDto2 {
    
    String storeName;
    String storeBanner;
    String storeLogo;
    String storeStatus;
    String storeAbout;

}
