package synrgy.thrifthouse.dto.rajaongkir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RajaOngkirError {
    private RajaOngkirStatus status;
}