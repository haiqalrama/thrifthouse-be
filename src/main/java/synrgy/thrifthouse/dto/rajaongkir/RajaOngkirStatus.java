package synrgy.thrifthouse.dto.rajaongkir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RajaOngkirStatus {
    private int code;
    private String description;
}
