package synrgy.thrifthouse.dto.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaginationProductDto<T> {
    private Long totalItems;
    private T products;
    private Integer totalPages;
    private Integer currentPage;
}
