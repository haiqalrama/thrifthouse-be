package synrgy.thrifthouse.dto.product;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import synrgy.thrifthouse.dto.store.StoreSimpleDto;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductWithStoreDto {
  private UUID id;

  private String name;

  private String brand;

  private String size;

  private String condition;

  private String photo;

  private Integer price;

  private Integer weight;

  private String category;

  private String subcategory1;

  private String subcategory2;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private StoreSimpleDto store;
}