package synrgy.thrifthouse.dto.product;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FavStoreDto {
  private UUID id;
  private String name;
  private String province;
  private String city;
  private String photo;
  private Integer totalProduct;
  private Integer totalReview;
  private Double averageReview;
  private Integer favoriteStore;
  
  public FavStoreDto(UUID id, String name, String province, String city, String photo) {
    this.id = id;
    this.name = name;
    this.province = province;
    this.city = city;
    this.photo = photo;
  }
}
