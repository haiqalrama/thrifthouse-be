package synrgy.thrifthouse.dto.product;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreInformationDto {
  private UUID id;
  private String name;
  private String province;
  private String province_id;
  private String city;
  private String city_id;
  private String logo;
  private String photo;
  private Integer totalProduct;
  private Integer totalReview;
  private Double averageReview;
  private Integer favoriteStore;
}
