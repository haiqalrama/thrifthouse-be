package synrgy.thrifthouse.dto.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductListDto {

    private UUID id;
    private String name;
    private String brand;
    private String size;
    private String condition;
    private Integer price;
    private String photos;
    private Boolean sold;
    private String category;
    private String subcategory1;
    private String subcategory2;
}
