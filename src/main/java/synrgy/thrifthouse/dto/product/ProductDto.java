package synrgy.thrifthouse.dto.product;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
  private UUID id;

  private String name;

  private String brand;

  private String size;

  private String condition;

  private Integer price;

  private Integer height;

  private Integer width;
  
  private Integer weight;

  private String material;

  private String description;

  private List<String> photos;

  private String category;

  private String subcategory1;

  private String subcategory2;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime createdAt;

  // private Store store;
}
