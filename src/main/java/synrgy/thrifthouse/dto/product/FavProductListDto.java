package synrgy.thrifthouse.dto.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FavProductListDto {

    private UUID id;
    private String name;
    private String brand;
    private String size;
    private String condition;
    private Integer price;
    private List<String> photos;
    private String category;
}
