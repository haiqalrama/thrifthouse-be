package synrgy.thrifthouse.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductIdDto {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UUID productId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonDeserialize()
    private List<UUID> productIds;
}
