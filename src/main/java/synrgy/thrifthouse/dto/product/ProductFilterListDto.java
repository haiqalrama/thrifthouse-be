package synrgy.thrifthouse.dto.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductFilterListDto {
    private String category;
    private String brand;
    private String size;
    private String condition;
}
