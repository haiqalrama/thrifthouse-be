package synrgy.thrifthouse.dto.user;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private UUID id;
    private String username;
    private String password;
    private String email;
    private String phone;
    private String profileImg;
    private String role;

    private String fullname;
    private Character gender;
    private String birth;

}
