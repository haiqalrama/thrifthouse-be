package synrgy.thrifthouse.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileRequestDto {

    private UUID id;
    private String username;
    private String fullname;
    private String email;
    private Character gender;
    private String phone;
    private Date birth;
    private MultipartFile profileImg;
    private String role;
}
