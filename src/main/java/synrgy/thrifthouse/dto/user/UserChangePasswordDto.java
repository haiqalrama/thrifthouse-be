package synrgy.thrifthouse.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserChangePasswordDto {

    String oldPassword;
    String newPassword;
    String confirmPassword;
}
