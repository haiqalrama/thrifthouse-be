package synrgy.thrifthouse.dto.user;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SellerAccountDto {
    
    private String fullname;
    private String email;
    private String phone;
    private Date birth;
    private Character gender;
    private String ktpNumber;
    private MultipartFile ktpImg;

}
