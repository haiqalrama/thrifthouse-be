package synrgy.thrifthouse.dto.notification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationDto {
    private UUID id;
    private UUID userId;
    private String title;
    private String body;
    private String type;
    private String image;
    private String status;
    private LocalDateTime notificationAt;
    private LocalDateTime readAt;
    private LocalDateTime updatedAt;
}
