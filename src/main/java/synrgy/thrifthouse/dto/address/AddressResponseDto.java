package synrgy.thrifthouse.dto.address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import synrgy.thrifthouse.dto.user.UserDto;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressResponseDto {

    private UUID id;
    private String recipientName;
    private String recipientPhone;
    private Boolean addressLabel;
    private Integer idProvince;
    private String province;
    private Integer idCity;
    private String city;
    private String district;
    private String village;
    private String fullAddress;
    private String postalCode;
    private String detail;
}