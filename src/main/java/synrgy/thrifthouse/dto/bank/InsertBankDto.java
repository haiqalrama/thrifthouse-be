package synrgy.thrifthouse.dto.bank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsertBankDto {
    
    private String bankName;
    private String bankNumber;
    private String bankHolder;
    private String bankLogo;

}
