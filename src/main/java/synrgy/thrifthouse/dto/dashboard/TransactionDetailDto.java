package synrgy.thrifthouse.dto.dashboard;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TransactionDetailDto {
    
    private UUID transactionId;
    private String buyerName;
    private UUID buyerId;
    private String pembayaranId;
    private String orderedAt;

    private int price;
    private int totalPrice;
    private String deliveryService;
    private Integer shippingCost;
    private String shippingService;
    private String estimatedTimeOfDeparture;

    private String phoneNumber;
    private String status;
    private String packagingStatus;
    private String receipt;

    private String addressDetail;
    private String province;
    private String city;
    private String district;
    private String village;
    private String postalCode;

    private List<OrderedProductDetailDto> products;
    

    public TransactionDetailDto(UUID transactionId, String buyerName, UUID buyerId, String pembayaranId, Date expiredAt,
            Integer price, String phoneNumber, String status, String packagingStatus, String receipt,
            String deliveryService, Integer shippingCost, String shippingService, String estimatedTimeOfDeparture,
            String addressDetail, String province, String city, String district, String village, String postalCode) {
        this.transactionId = transactionId;
        this.buyerName = buyerName;
        this.buyerId = buyerId;
        this.pembayaranId = pembayaranId;
        this.price = price;
        this.phoneNumber = phoneNumber;
        this.status = status;
        this.packagingStatus = packagingStatus;
        this.deliveryService = deliveryService;
        this.shippingCost = shippingCost;
        this.shippingService = shippingService;
        this.estimatedTimeOfDeparture = estimatedTimeOfDeparture;
        this.receipt = receipt;
        this.addressDetail = addressDetail;
        this.province = province;
        this.city = city;
        this.district = district;
        this.village = village;
        this.postalCode = postalCode;

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMMMMMM yyyy", new Locale("id", "ID"));
        Date orderedAt_Date = new Date(expiredAt.getTime() - 86400000L);
        this.orderedAt = formatter.format(orderedAt_Date);
    }
    
}
