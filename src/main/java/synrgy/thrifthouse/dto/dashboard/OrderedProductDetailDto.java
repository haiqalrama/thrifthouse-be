package synrgy.thrifthouse.dto.dashboard;

import java.util.UUID;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderedProductDetailDto {
    
    private UUID productId;
    private String productName;
    private Integer quantity = 1;
    private Integer price;
    
    public OrderedProductDetailDto(UUID productId, String productName, Integer price) {
        this.productId = productId;
        this.productName = productName;
        this.price = price;
    }
    
}
