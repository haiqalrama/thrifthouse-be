package synrgy.thrifthouse;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
import synrgy.thrifthouse.service.ImageService;

@EnableSwagger2
@SpringBootApplication
public class ThrifthouseApplication implements CommandLineRunner {
	@Resource
	ImageService imageService;

	public static void main(String[] args) {
		SpringApplication.run(ThrifthouseApplication.class, args);
	}
	
	@Override
	public void run(String... arg) throws Exception {
		imageService.init();
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
}
