package synrgy.thrifthouse.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import synrgy.thrifthouse.dto.product.ProductWithStoreDto;
import synrgy.thrifthouse.dto.review.ReviewDto;
import synrgy.thrifthouse.dto.review.ReviewList;
import synrgy.thrifthouse.dto.review.ReviewListDto;
import synrgy.thrifthouse.dto.review.ReviewPostDto;
import synrgy.thrifthouse.model.Product;
import synrgy.thrifthouse.model.Review;

@Mapper(componentModel = "spring")
public interface ReviewMapper {

    ReviewMapper INSTANCE = Mappers.getMapper(ReviewMapper.class);

    ReviewDto ReviewToReviewDto(Review review);

    Review ReviewDtoToReview(ReviewDto reviewDto);

    ReviewListDto ReviewListToReviewListDto(ReviewList reviewList);

    @Named("getPhoto")
    public static String getPhoto(Product product) {
        return product.getPhotos().get(0);
    }

    @Mapping(target = "photos", ignore = true)
    Review ReviewPostDtoToReview(ReviewPostDto reviewPostDto);
}
