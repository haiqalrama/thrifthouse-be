package synrgy.thrifthouse.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import synrgy.thrifthouse.dto.address.AddressResponseDto;
import synrgy.thrifthouse.model.Address;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    AddressResponseDto AddressToAddressResponseDto(Address address);
}

