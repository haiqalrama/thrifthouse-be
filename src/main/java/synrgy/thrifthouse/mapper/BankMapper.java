package synrgy.thrifthouse.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import synrgy.thrifthouse.dto.bank.BankDto;
import synrgy.thrifthouse.dto.bank.InsertBankDto;
import synrgy.thrifthouse.model.Bank;

@Mapper(componentModel = "spring")
public interface BankMapper {

    BankMapper INSTANCE = Mappers.getMapper(BankMapper.class);

    BankDto BankToBankDto(Bank bank);

    Bank BankDtoToBank(InsertBankDto bankDto);

}
