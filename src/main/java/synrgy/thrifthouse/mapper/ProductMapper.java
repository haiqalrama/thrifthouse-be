package synrgy.thrifthouse.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import synrgy.thrifthouse.dto.product.*;
import synrgy.thrifthouse.model.Product;

import java.util.UUID;

@Mapper(componentModel = "spring")
public interface ProductMapper {
  ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

  @Mapping(source = "createdAt", target = "createdAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
  ProductDto ProductToProductDto(Product product);

  @Mapping(source = "createdAt", target = "createdAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
  Product ProductDtoToProduct(ProductDto productDto);

  @Mapping(source = "product", target = "photos", qualifiedByName = "getPhoto")
  ProductListDto ProductToProductListDto(Product product);

  @Named("getPhoto")
  public static String getPhoto(Product product) {
    return product.getPhotos().get(0);
  }

  @Mapping(source = "product", target = "photo", qualifiedByName = "getPhoto")
  ProductWithStoreDto ProductToProductWithStoreDto(Product product);

  @Mapping(source = "product", target = "productId", qualifiedByName = "getFavoriteProductId")
  ProductFavoriteDto ProductToProductFavoriteDto(Product product);
  @Named("getFavoriteProductId")
  public static UUID getFavoriteStoreId(Product product) {
    return product.getId();
  }

  @Mapping(target = "photos", ignore = true)
  Product productRequestDtoToProduct(ProductRequestDto productRequestDto);

  ProductDto productToProductDto(Product product);
}
