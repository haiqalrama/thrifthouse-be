package synrgy.thrifthouse.model;

import javax.persistence.*;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "favorite_product")
public class FavoriteProduct {
    
    @EmbeddedId
    private FavoriteProductKey id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @MapsId("productId")
    @JoinColumn(name = "product_id")
    private Product product;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP", name = "created_at", updatable = false)
    private LocalDateTime createdAt;

    public FavoriteProduct(FavoriteProductKey id, User user, Product product) {
        this.id = id;
        this.user = user;
        this.product = product;
    }

}
