package synrgy.thrifthouse.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class FavoriteProductKey implements Serializable{
    
    @Column(name = "user_id")
    private UUID userId;

    @Column(name = "product_id")
    private UUID productId;

}
