package synrgy.thrifthouse.model;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "store")
public class Store {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id", referencedColumnName = "id", unique = true)
    private User user;

    @Column(name = "name")
    private String name;

    @Column(name = "province")
    private String province;
    
    @Column(name = "city")
    private String city;
    
    @Lob
    @Column(name = "about")
    private String about;
    
    @Lob
    @Column(name = "banner")
    private String banner;

    @Lob
    @Column(name = "logo")
    private String logo;
    
    @Lob
    @Column(name = "photo")
    private String photo;

    @Column(name = "store_status")
    private String storeStatus = "buka";

    @Column(name = "full_address")
    private String fullAddress;

    @Column(name = "ktp_number")
    private String ktpNumber;

    @Lob
    @Column(name = "ktp_img")
    private String ktpImg;

    @Column(name = "delivery_service")
    private String deliveryService;

    @Column(name = "bank_holder")
    private String bankHolder;

    @Column(name = "bank_number")
    private String bankNumber;

    @Column(name = "bank_name")
    private String bankName;

    @OneToMany(mappedBy = "store")
    private Collection<Review> reviews;
    
    @OneToMany(mappedBy = "store")
    private Collection<Product> products;

    @OneToMany(mappedBy = "store")
    private Collection<FavoriteStore> favorites;

    public Store(UUID id, String name, String about, String banner, String logo, String storeStatus) {
        this.id = id;
        this.name = name;
        this.about = about;
        this.banner = banner;
        this.logo = logo;
        this.storeStatus = storeStatus;
    }

}
