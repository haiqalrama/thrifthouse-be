package synrgy.thrifthouse.model;

import javax.persistence.*;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "favorite")
public class FavoriteStore {
    
    @EmbeddedId
    private FavoriteStoreKey id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @MapsId("storeId")
    @JoinColumn(name = "store_id")
    private Store store;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP", name = "created_at", updatable = false)
    private LocalDateTime createdAt;

    public FavoriteStore(FavoriteStoreKey id, User user, Store store) {
        this.id = id;
        this.user = user;
        this.store = store;
    }

}
