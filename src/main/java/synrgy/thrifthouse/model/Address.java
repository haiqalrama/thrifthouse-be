package synrgy.thrifthouse.model;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "address")
public class Address {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "address")
    private Collection<Order> orders;

    @Column(name = "recipient_name")
    private String recipientName;

    @Column(name = "recipient_phone")
    private String recipientPhone;

    @Column(name = "address_label")
    private Boolean addressLabel;

    @Column(name = "id_province")
    private Integer idProvince;

    @Column(name = "province")
    private String province;

    @Column(name = "idCity")
    private Integer idCity;

    @Column(name = "city")
    private String city;

    @Column(name = "district")
    private String district;

    @Column(name = "village")
    private String village;

    @Column(name = "full_address")
    private String fullAddress;

    @Column(name = "postal_code")
    private String postalCode;

    @Lob
    @Column(name = "detail")
    private String detail;

}
