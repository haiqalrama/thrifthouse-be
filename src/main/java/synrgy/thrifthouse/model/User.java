package synrgy.thrifthouse.model;

import java.awt.*;
import java.util.*;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "\"user\"")
@DynamicUpdate
public class User {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Lob
    @Column(name = "profile_img")
    private String profileImg;

    @Column(name = "phone")
    private String phone;

    @Column(name = "role")
    private String role;

    @Column(name = "birth")
    private Date birth;

    @Column(name = "gender")
    private Character gender;
    
    @OneToOne(mappedBy = "user")
    private Store store;

    @OneToMany(mappedBy = "user")
    private Collection<Address> addresses;

    @OneToMany(mappedBy = "user")
    private Collection<Review> reviews;

    @OneToMany(mappedBy = "user")
    private Collection<FavoriteStore> stores;

    @OneToMany(mappedBy = "user")
    private Collection<FavoriteProduct> favoriteProducts;

    @ManyToMany
    @JoinTable(
        name = "cart", 
        joinColumns = @JoinColumn(name = "user_id"), 
        inverseJoinColumns = @JoinColumn(name = "product_id"))
    private Collection<Product> cart;

    @OneToMany(mappedBy = "user")
    private Collection<Order> orders;

    @OneToMany(mappedBy = "user")
    private Collection<Notification> notifications = new ArrayList<>();

    public User(UUID id, String username, String password, String email, String phone, String role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.role = role;
    }

    @PrePersist
    public void insertUserProfile() {
        Random rand = new Random();
        Color color = new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
        color.darker();
        if (this.username!=null) {
            if (!this.username.isEmpty()) {
                this.profileImg = "https://ui-avatars.com/api/?background="+String.format("%06x", color.getRGB() & 0x00FFFFFF)+"&color=fff&name="+this.username+"&size=64";
            } else {
                this.profileImg = "https://ui-avatars.com/api/?background="+String.format("%06x", color.getRGB() & 0x00FFFFFF)+"&color=fff&name=thriftUser&size=64";
            }
        } else {
            this.profileImg = "https://ui-avatars.com/api/?background="+String.format("%06x", color.getRGB() & 0x00FFFFFF)+"&color=fff&name=thriftUser&size=64";
        }
    }

}
