package synrgy.thrifthouse.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import synrgy.thrifthouse.model.User;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByRole(String role);
    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);

}
