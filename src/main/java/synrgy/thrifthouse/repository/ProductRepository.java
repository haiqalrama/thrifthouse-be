package synrgy.thrifthouse.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import synrgy.thrifthouse.dto.product.ProductFilterListDto;
import synrgy.thrifthouse.dto.product.ProductListDto;
import synrgy.thrifthouse.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID>, JpaSpecificationExecutor<Product> {

    @Query(value = "SELECT p FROM Product p WHERE lower(p.name) LIKE %?1%")
    Page<Product> getAll(String search, Pageable pageable);

    @Query(value = "SELECT p FROM Product p " +
            "WHERE p.category = ?1 OR p.store.id = ?2")
    Page<Product> getListByCategoryOrStoreId(String category, UUID storeId, Pageable pageable);

//     @Query(value = "SELECT new synrgy.thrifthouse.dto.product.ProductListDto(p.id, p.name, p.brand, p.size, p.condition, p.price, p.photos, p.category, p.subcategory1, p.subcategory2) "
//             + "FROM Product p INNER JOIN p.favoriteUser fu WHERE fu.id = ?1 AND lower(p.name) LIKE %?2% ")
//     Page<ProductListDto> getListFavorite(UUID userId, String search, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.product.ProductListDto(p.id, p.name, p.brand, p.size, p.condition, p.price, p.photos, p.sold, p.category, p.subcategory1, p.subcategory2) " +
    "FROM Product p " +
    "JOIN FavoriteProduct fp " +
    "ON fp.product = p.id " +
    "JOIN User u " +
    "ON fp.user = u.id " +
    "WHERE u.id = ?1 AND LOWER(p.name) LIKE %?2% " +
    "ORDER BY fp.createdAt DESC")
    Page<ProductListDto> getListFavoriteDesc(UUID userId, String search, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.product.ProductListDto(p.id, p.name, p.brand, p.size, p.condition, p.price, p.photos, p.sold, p.category, p.subcategory1, p.subcategory2) " +
    "FROM Product p " +
    "JOIN FavoriteProduct fp " +
    "ON fp.product = p.id " +
    "JOIN User u " +
    "ON fp.user = u.id " +
    "WHERE u.id = ?1 AND LOWER(p.name) LIKE %?2% " +
    "ORDER BY fp.createdAt ASC")
    Page<ProductListDto> getListFavoriteAsc(UUID userId, String search, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.product.ProductListDto(p.id, p.name, p.brand, p.size, p.condition, p.price, p.photos, p.sold, p.category, p.subcategory1, p.subcategory2) " +
    "FROM Product p " +
    "JOIN FavoriteProduct fp " +
    "ON fp.product = p.id " +
    "JOIN User u " +
    "ON fp.user = u.id " +
    "WHERE u.id = ?1 AND LOWER(p.name) LIKE %?2% " +
    "ORDER BY p.name DESC")
    Page<ProductListDto> getListFavoriteAlphabeticalDesc(UUID userId, String search, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.product.ProductListDto(p.id, p.name, p.brand, p.size, p.condition, p.price, p.photos, p.sold, p.category, p.subcategory1, p.subcategory2) " +
    "FROM Product p " +
    "JOIN FavoriteProduct fp " +
    "ON fp.product = p.id " +
    "JOIN User u " +
    "ON fp.user = u.id " +
    "WHERE u.id = ?1 AND LOWER(p.name) LIKE %?2% " +
    "ORDER BY p.name ASC")
    Page<ProductListDto> getListFavoriteAlphabeticalAsc(UUID userId, String search, Pageable pageable);

    @Query("SELECT new synrgy.thrifthouse.dto.product.ProductFilterListDto(p.category, p.brand, p.size, p.condition) " +
            "FROM Product p GROUP BY p.category, p.brand, p.size, p.condition")
    List<ProductFilterListDto> findFilters();
    
}
