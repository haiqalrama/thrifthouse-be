package synrgy.thrifthouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import synrgy.thrifthouse.model.PasswordReset;

import java.util.Optional;
import java.util.UUID;

public interface PasswordResetRepository extends JpaRepository<PasswordReset, UUID> {
    Optional<PasswordReset> findByToken(String token);
}
