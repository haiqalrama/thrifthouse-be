package synrgy.thrifthouse.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import synrgy.thrifthouse.dto.product.FavStoreDto;
import synrgy.thrifthouse.dto.store.ProductsOfStoreDto;
import synrgy.thrifthouse.model.Store;

public interface StoreRepository extends JpaRepository<Store, UUID> {

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.store.ProductsOfStoreDto(s.id, p.id, p.name, p.brand, p.size, p.condition, p.price, p.photos, p.category) " +
    "FROM Product p " +
    "JOIN Store s " +
    "ON p.store = s.id " +
    "WHERE (s.id = ?1 AND p.category = ?2)")
    List<ProductsOfStoreDto> getProductsByStoreIdAndCategory(UUID id, String category, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.store.ProductsOfStoreDto(s.id, p.id, p.name, p.brand, p.size, p.condition, p.price, p.photos, p.category) " +
    "FROM Product p " +
    "JOIN Store s " +
    "ON p.store = s.id " +
    "WHERE s.id = ?1")
    List<ProductsOfStoreDto> getProductsByStoreId(UUID id, Pageable pageable);

    // @Query(value = "SELECT s FROM Store s INNER JOIN s.favorites f WHERE f.id = ?1 AND lower( s.name ) LIKE %?2% ")
    // Page<Store> getListFavorite(UUID userId, String search, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.product.FavStoreDto(s.id, s.name, s.province, s.city, s.photo) " +
    "FROM Store s " +
    "JOIN FavoriteStore fs " +
    "ON fs.store = s.id " +
    "JOIN User u " +
    "ON fs.user = u.id " +
    "WHERE u.id = ?1 AND LOWER(s.name) LIKE %?2% " +
    "ORDER BY fs.createdAt DESC")
    Page<FavStoreDto> getListFavoriteDesc(UUID userId, String search, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.product.FavStoreDto(s.id, s.name, s.province, s.city, s.photo) " +
    "FROM Store s " +
    "JOIN FavoriteStore fs " +
    "ON fs.store = s.id " +
    "JOIN User u " +
    "ON fs.user = u.id " +
    "WHERE u.id = ?1 AND LOWER(s.name) LIKE %?2% " +
    "ORDER BY fs.createdAt ASC")
    Page<FavStoreDto> getListFavoriteAsc(UUID userId, String search, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.product.FavStoreDto(s.id, s.name, s.province, s.city, s.photo) " +
    "FROM Store s " +
    "JOIN FavoriteStore fs " +
    "ON fs.store = s.id " +
    "JOIN User u " +
    "ON fs.user = u.id " +
    "WHERE u.id = ?1 AND LOWER(s.name) LIKE %?2% " +
    "ORDER BY s.name DESC")
    Page<FavStoreDto> getListFavoriteAlphabeticalDesc(UUID userId, String search, Pageable pageable);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.product.FavStoreDto(s.id, s.name, s.province, s.city, s.photo) " +
    "FROM Store s " +
    "JOIN FavoriteStore fs " +
    "ON fs.store = s.id " +
    "JOIN User u " +
    "ON fs.user = u.id " +
    "WHERE u.id = ?1 AND LOWER(s.name) LIKE %?2% " +
    "ORDER BY s.name ASC")
    Page<FavStoreDto> getListFavoriteAlphabeticalAsc(UUID userId, String search, Pageable pageable);

    Optional<Store> findByUserId(UUID userId);

    boolean existsByUser_Id(UUID id);
}
