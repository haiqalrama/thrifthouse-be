package synrgy.thrifthouse.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import synrgy.thrifthouse.model.Bank;

public interface BankRepository extends JpaRepository<Bank, UUID> {
    
}
