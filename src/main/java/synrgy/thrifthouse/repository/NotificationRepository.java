package synrgy.thrifthouse.repository;

import org.mapstruct.Mapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import synrgy.thrifthouse.model.Notification;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, UUID> {
    Optional<Notification> findByIdAndUser_Id(UUID id, UUID id1);

    List<Notification> findByUser_IdAndTypeAllIgnoreCaseOrderByUpdatedAtDesc(UUID userId, String type);

    List<Notification> findByUser_IdAllIgnoreCaseOrderByUpdatedAtDesc(UUID id);
}
