package synrgy.thrifthouse.repository.FilterProduct;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class Filter {
    private String field;
    private FilterProductRepository.QueryOperator operator;
    private String value;
    private List<String> values;
    private String joinField;
}