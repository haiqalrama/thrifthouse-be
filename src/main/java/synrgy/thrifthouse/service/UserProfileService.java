package synrgy.thrifthouse.service;

import synrgy.thrifthouse.dto.user.UserChangePasswordDto;
import synrgy.thrifthouse.dto.user.UserDto;
import synrgy.thrifthouse.dto.user.UserProfileRequestDto;

import java.util.Map;
import java.util.UUID;

public interface UserProfileService {

    UserDto updateProfile(UUID userid, UserProfileRequestDto userProfileRequestDto);
    Map<String, UUID> updatePassword(UUID userid, UserChangePasswordDto userChangePasswordDto);
}
