package synrgy.thrifthouse.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

import synrgy.thrifthouse.dto.product.FavStoreDto;
import synrgy.thrifthouse.dto.product.StoreInformationDto;
import synrgy.thrifthouse.dto.store.*;
import synrgy.thrifthouse.dto.storeSettings.InformationDto;
import synrgy.thrifthouse.dto.storeSettings.InformationDto2;
import synrgy.thrifthouse.dto.storeSettings.StoreAddressDto;
import synrgy.thrifthouse.dto.storeSettings.StoreBankDto;
import synrgy.thrifthouse.dto.storeSettings.StoreDeliveryServiceDto;
import synrgy.thrifthouse.dto.user.SellerAccountDto;
import synrgy.thrifthouse.dto.user.SellerAccountDto2;

public interface StoreService {
    
    StoreDetailBannerDto getBannerByStoreId(UUID id);
    Map<String, String> getLogoByStoreId(UUID id);
    StoreInformationDto getStoreInformationById(UUID id) throws JsonMappingException, JsonProcessingException, IOException;
    List<ProductsOfStoreDto> getStoreProducts(Integer page, Integer size, UUID id, String category);
    Object getStoreAbout(UUID id);

    // Favorite Store
    FavoriteStoreDto favoriteStore (UUID userId, FavoriteStoreDto favoriteStoreDto);
    PaginationStoreDto<List<FavStoreDto>> getAllFavoriteStore(UUID userId, String Search, String urutkan, Integer page, Integer size);

    ObjectNode createStore(UUID userId, StoreCreatePostDto storeCreatePostDto);

    // Store settings
    InformationDto2 getStoreInformation(UUID storeId);
    InformationDto2 updateStoreInformation(UUID storeId, InformationDto informationDto);
    StoreBankDto getStoreBankAccount(UUID storeId);
    StoreBankDto updateStoreBankAccount(UUID storeId, StoreBankDto storeBankDto);
    StoreAddressDto getStoreAddress(UUID storeId);
    StoreAddressDto updateStoreAddress(UUID storeId, StoreAddressDto storeAddressDto);
    StoreDeliveryServiceDto getStoreDeliveryService(UUID id);
    StoreDeliveryServiceDto updateStoreDeliveryService(UUID storeId, StoreDeliveryServiceDto storeDeliveryServiceDto);

    // Seller Profile
    SellerAccountDto2 getSellerDetail(UUID userId);
    SellerAccountDto2 updateSellerDetail(UUID userId, SellerAccountDto sellerAccount);
    StoreKtpDto getStoreKtp(UUID store_id);
}
