package synrgy.thrifthouse.service;

import java.util.List;

import synrgy.thrifthouse.dto.bank.BankDto;
import synrgy.thrifthouse.dto.bank.InsertBankDto;

public interface BankService {
    
    List<BankDto> getBanks();
    void insertBank(InsertBankDto bankDto);

}
