package synrgy.thrifthouse.service;

import org.springframework.web.multipart.MultipartFile;
import synrgy.thrifthouse.dto.review.ReviewDto;
import synrgy.thrifthouse.dto.review.ReviewListDto;
import synrgy.thrifthouse.dto.review.ReviewPostDto;

import java.util.List;
import java.util.UUID;

public interface ReviewService {
    public ReviewListDto getReviews(Integer page, Integer size, Integer rate, Boolean hasPhoto, UUID storeId);

    ReviewDto postReview(UUID storeId, ReviewPostDto reviewPostDto, List<MultipartFile> photos);
}
