package synrgy.thrifthouse.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pusher.rest.Pusher;
import org.springframework.stereotype.Service;
import synrgy.thrifthouse.dto.notification.NotificationDto;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.mapper.NotificationMapper;
import synrgy.thrifthouse.model.Notification;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.NotificationRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.NotificationService;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;
    private final NotificationMapper notificationMapper;
    private final UserRepository userRepository;
    private final Pusher pusher;
    private final ObjectMapper objectMapper;

    public NotificationServiceImpl(NotificationRepository notificationRepository, NotificationMapper notificationMapper, UserRepository userRepository, Pusher pusher, ObjectMapper objectMapper) {
        this.notificationRepository = notificationRepository;
        this.notificationMapper = notificationMapper;
        this.userRepository = userRepository;
        this.pusher = pusher;
        this.objectMapper = objectMapper;
    }

    @Override
    public List<NotificationDto> getAllNotificationByUserId(UUID userId, String type) {
        boolean exists = userRepository.existsById(userId);
        if (!exists) {
            throw new ResourceNotFoundException("User was not found");
        }

        List<Notification> all;
        if (type != null) {
            switch (type.toLowerCase(Locale.ROOT)) {
                case "pesanan":
                    all = notificationRepository.findByUser_IdAndTypeAllIgnoreCaseOrderByUpdatedAtDesc(userId, type);
                    break;
                case "info":
                    all = notificationRepository.findByUser_IdAndTypeAllIgnoreCaseOrderByUpdatedAtDesc(userId, type);
                    break;
                default:
                    all = notificationRepository.findByUser_IdAllIgnoreCaseOrderByUpdatedAtDesc(userId);
                    break;
            }
        } else {
            all = notificationRepository.findByUser_IdAllIgnoreCaseOrderByUpdatedAtDesc(userId);
        }
        return notificationMapper.mapsNotificationsToNotificationDtos(all);
    }

    @Override
    public NotificationDto markAsRead(UUID userId, UUID notificationId) {
        boolean userExists = userRepository.existsById(userId);
        if (!userExists) {
            throw new ResourceNotFoundException("User was not found");
        }
        Notification notification = notificationRepository.findByIdAndUser_Id(notificationId, userId)
                .orElseThrow(()-> new ResourceNotFoundException("Notification was not found"));

        notification.setReadAt(LocalDateTime.now());
        notificationRepository.save(notification);

        return notificationMapper.NotificationToNotificationDto(notification);
    }

    @Override
    public NotificationDto createNotification(UUID userId, NotificationDto notificationDto) {
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User was not found"));
        Notification notification = notificationMapper.NotificationDtoToNotification(notificationDto);
        notification.setUser(user);
        notificationRepository.save(notification);
        NotificationDto notificationDto1 = notificationMapper.NotificationToNotificationDto(notification);
        Map<String, String> map = objectMapper.convertValue(notificationDto1, new TypeReference<Map<String, String>>() {});
        pusher.trigger(
                "thrifthouse",
                user.getId().toString(),
                map
        );
        return notificationMapper.NotificationToNotificationDto(notification);
    }
}
