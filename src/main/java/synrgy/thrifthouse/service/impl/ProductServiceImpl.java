package synrgy.thrifthouse.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import synrgy.thrifthouse.dto.product.*;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.mapper.ProductMapper;
import synrgy.thrifthouse.mapper.StoreMapper;
import synrgy.thrifthouse.model.FavoriteProduct;
import synrgy.thrifthouse.model.FavoriteProductKey;
import synrgy.thrifthouse.model.Product;
import synrgy.thrifthouse.model.Store;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.FilterProduct.FilterProductRepository;
import synrgy.thrifthouse.repository.FavoriteProductRepository;
import synrgy.thrifthouse.repository.ProductRepository;
import synrgy.thrifthouse.repository.ReviewRepository;
import synrgy.thrifthouse.repository.StoreRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.ImageService;
import synrgy.thrifthouse.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;
  private final FilterProductRepository filterProductRepository;
  private final UserRepository userRepository;
  private final StoreRepository storeRepository;
  private final FavoriteProductRepository favoriteProductRepository;
  private final ProductMapper productMapper;
  private final StoreMapper storeMapper;

  @Autowired
  private ImageService imageService;

  @Autowired
  public ProductServiceImpl(ProductRepository productRepository, FilterProductRepository filterProductRepository, StoreRepository storeRepository,
                            ReviewRepository reviewRepository, ProductMapper productMapper,
                            StoreMapper storeMapper, UserRepository userRepository, FavoriteProductRepository favoriteProductRepository) {
    this.productRepository = productRepository;
    this.filterProductRepository = filterProductRepository;
    this.storeRepository = storeRepository;
    this.productMapper = productMapper;
    this.storeMapper = storeMapper;
    this.userRepository = userRepository;
    this.favoriteProductRepository = favoriteProductRepository;
  }

  @Override
  public ProductDetailDto findDetail(UUID productId) throws IOException {
    Optional<Product> product = productRepository.findById(productId);
    if (product.isPresent()) {
      UUID id = product.get().getStore().getId();
      Optional<Store> store = storeRepository.findById(id);

      if (store.isPresent()) {
        StoreInformationDto storeInfo = storeMapper.StoreToStoreInformationDto(store.get());
        ProductDto productDto = productMapper.ProductToProductDto(product.get());
        ObjectMapper objectMapper = new ObjectMapper();

        Path name = Paths.get("src/main/resources/get-city-response.json");
        JsonNode cityResponse = objectMapper.readTree(
          new String(Files.readAllBytes(Paths.get(name.toUri()))).replaceAll("[\t\r\n]", "")
        );

        String cityId = "";
        String provinceId = "";
        boolean isFound = false;
        for (JsonNode result : cityResponse.path("rajaongkir").path("results")) {
          // store the city and province name
          String cityName = result.path("city_name").toString().split("\"")[1];
          String provinceName = result.path("province").toString().split("\"")[1];
          if (cityName.equals(storeInfo.getCity()) && provinceName.equals(storeInfo.getProvince())) {
            provinceId = result.path("province_id").toString().split("\"")[1];
            cityId = result.path("city_id").toString().split("\"")[1];
            isFound = true;
            break;
          }
        }
        if (!isFound) {
          storeInfo.setProvince_id("INVALID_NAME");
          storeInfo.setCity_id("INVALID_NAME");
        } else {
          storeInfo.setCity_id(cityId);
          storeInfo.setProvince_id(provinceId);
        }

        return new ProductDetailDto(storeInfo, productDto);
      } else {
        return null;
      }
    } else {
      throw new ResourceNotFoundException("Product with id " + productId + " not found");
    }
  }

  @Override
  public PaginationProductDto<List<ProductListDto>> getAll(Integer page, Integer size, String sortBy, ProductFilter productFilter) {
    Sort sort;
    if (sortBy!=null) {
      switch (sortBy) {
        case "latest":
          sort = Sort.by(Sort.Direction.DESC, "createdAt");
          break;
        case "highest":
          sort = Sort.by(Sort.Direction.DESC, "price");
          break;
        case "lowest":
          sort = Sort.by(Sort.Direction.ASC, "price");
          break;
        default:
          throw new IllegalArgumentException("sortBy value is unsupported yet.");
      }
    } else {
      sort = Sort.by(Sort.Direction.DESC, "createdAt");
    }
    Pageable paging = PageRequest.of(page, size, sort);

    Page<Product> productPage;
    try {
      productPage = this.filterProductRepository.findAllByFilter(productFilter, paging);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e.getMessage());
    }

    List<ProductListDto> productListDtos = productPage.stream()
            .map(productMapper::ProductToProductListDto)
            .collect(Collectors.toList());

    return new PaginationProductDto<>(productPage.getTotalElements(), productListDtos, productPage.getTotalPages(), productPage.getNumber());
  }

  @Override
  public Map<String, Set<Object>> getFilters() {
    List<ProductFilterListDto> all = productRepository.findFilters();

    Map<String, Set<Object>> filters = new HashMap<>();

    Set<String> categories = all.stream().map(ProductFilterListDto::getCategory).collect(Collectors.toSet());
    Set<String> brands = all.stream().map(ProductFilterListDto::getBrand).collect(Collectors.toSet());
    Set<String> sizes = all.stream().map(ProductFilterListDto::getSize).collect(Collectors.toSet());
    Set<String> conditions = all.stream().map(ProductFilterListDto::getCondition).collect(Collectors.toSet());

    filters.put("categories", new HashSet<>(categories));
    filters.put("brands", new HashSet<>(brands));
    filters.put("sizes", new HashSet<>(sizes));
    filters.put("conditions", new HashSet<>(conditions));

    return filters;
  }

  @Override
  public ProductDto storeProduct(ProductRequestDto productRequestDto, UUID sellerId) {

    final Optional<Store>storeByUserId = storeRepository.findByUserId(sellerId);

    if(!storeByUserId.isPresent()){
      throw new ResourceNotFoundException("Store with userId " + sellerId+ " not found");
    }

    // Save in DB
    Product product = productMapper.productRequestDtoToProduct(productRequestDto);
//    product.setPhotos(namePhotos);
    product.setStore(storeByUserId.get());
    Product productSave = productRepository.save(product);

    int imgNumber = 1;
    List<String> namePhotos = new ArrayList<>();
    for (MultipartFile photo: productRequestDto.getPhotos()) {
      int getIndex     = photo.getOriginalFilename().lastIndexOf('.');
      String extension = photo.getOriginalFilename().substring(getIndex);
      String newName   = "product_"+ productSave.getId()+"_" + "img"+imgNumber+"_"+ this.getDateNow() + extension;
      namePhotos.add(imageService.saveImage(photo, "/products/" + newName ));
      imgNumber++;
    }

    // Update Fiel Product Photos
    productSave.setPhotos(namePhotos);
    productRepository.save(productSave);

    return productMapper.ProductToProductDto(productSave);
  }

  public String getDateNow(){
    Date date = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
    formatter.setTimeZone(TimeZone.getTimeZone("GMT+7"));
    return formatter.format(date);
  }

  @Override
  public ProductFavoriteDto deleteProduct(UUID id) {

    final Optional<Product> product = productRepository.findById(id);
    if(!product.isPresent()){
      throw new ResourceNotFoundException("Product with " + id + " not found");
    }

    product.get().getPhotos().forEach(photo -> {
      int i = photo.lastIndexOf("/");
      imageService.deleteImage("products/" + photo.substring(++i));
    });

    productRepository.deleteById(id);
    ProductFavoriteDto productFavoriteDto = new ProductFavoriteDto();
    productFavoriteDto.setProductId(id);
    return productFavoriteDto;
  }

  @Override
  public ProductDto getById(UUID id) {
    final Optional<Product> product = productRepository.findById(id);

    if(!product.isPresent()){
      throw new ResourceNotFoundException("Product with " + id + " not found");
    }
    return productMapper.productToProductDto(product.get()) ;
  }

  @Override
  public ProductDto updateProduct(UUID productId, ProductRequestDto productRequestDto, int[] changePhoto) {

    final Optional<Product> product = productRepository.findById(productId);
    if (!product.isPresent()) {
      throw new ResourceNotFoundException("Product with " + productId + " not found");
    }

    List<String> newNamePhotos = product.get().getPhotos();

    if (changePhoto != null) {
      //Get name photo
      int imgNumber = 0;
      List<String> namePhotos = new ArrayList<>();
      for (MultipartFile photo: productRequestDto.getPhotos()) {
        int getIndex     = photo.getOriginalFilename().lastIndexOf('.');
        String extension = photo.getOriginalFilename().substring(getIndex);
        String newName   = "product_"+ productId +"_" + "img"+ (changePhoto[imgNumber] + 1) +"_"+ this.getDateNow() + extension;
        namePhotos.add(imageService.saveImage(photo, "/products/" + newName ));
        imgNumber++;
      }

      // Change photo in photos DB
      for (int i = 0; i < changePhoto.length; i++) {
            newNamePhotos.set(changePhoto[i], namePhotos.get(i));
      }
    }

    Product newProduct = productMapper.productRequestDtoToProduct(productRequestDto);
    newProduct.setId(productId);
    newProduct.setPhotos(newNamePhotos);
    newProduct.setStore(product.get().getStore());

    return productMapper.productToProductDto(productRepository.save(newProduct));
  }

  @Override
  public PaginationProductDto<List<ProductListDto>> getAllFavoriteProduct(UUID userId, String search, String urutkan, Integer page, Integer size ) {
    final Optional<User> user = userRepository.findById(userId);
    if(!user.isPresent()){
      throw new ResourceNotFoundException("Buyer with userId " + userId + " was not found");
    }

    Pageable paging = PageRequest.of(page, size);

    Page<ProductListDto> productListPage = null;
    List<ProductListDto> listFavorite = null;
    switch(urutkan) {
      case "terbaru_disimpan":
        productListPage = productRepository.getListFavoriteDesc(userId, search.toLowerCase(), paging);
        listFavorite = productListPage.stream().collect(Collectors.toList());
        break;
      case "terakhir_disimpan":
        productListPage = productRepository.getListFavoriteAsc(userId, search.toLowerCase(), paging);
        listFavorite = productListPage.stream().collect(Collectors.toList());
        break;
      case "abjad_awal":
        productListPage = productRepository.getListFavoriteAlphabeticalAsc(userId, search.toLowerCase(), paging);
        listFavorite = productListPage.stream().collect(Collectors.toList());
        break;
      case "abjad_akhir":
        productListPage = productRepository.getListFavoriteAlphabeticalDesc(userId, search.toLowerCase(), paging);
        listFavorite = productListPage.stream().collect(Collectors.toList());
        break;
    }

    for (ProductListDto dto : listFavorite) {
      String photosArray = dto.getPhotos();
      String firstPhoto = photosArray.substring(1, photosArray.length() - 1).split(", ")[0];
      dto.setPhotos(firstPhoto);
    }
    return new PaginationProductDto<>(productListPage.getTotalElements() , listFavorite, productListPage.getTotalPages(), productListPage.getNumber());
  }

  public boolean contains(final int[] arr, final int key){
    return ArrayUtils.contains(arr, key);
  }

  @Override
  public ProductFavoriteDto storeFavoriteProduct(UUID userId, ProductFavoriteDto productFavoriteDto) {
    final Optional<User> userOptional = userRepository.findById(userId);
    final Optional<Product> productOptional = productRepository.findById(productFavoriteDto.getProductId());

    if(!userOptional.isPresent()){
      throw new ResourceNotFoundException("Store with userId " + userId + " not found");
    }
    if(!productOptional.isPresent()){
      throw new ResourceNotFoundException("ProductId " + productFavoriteDto.getProductId() + " not found");
    }

    User user = userOptional.get();
    Product product = productOptional.get();

    // Compare productId in DB with productId Params
    
    final List<FavoriteProduct> productList = user.getFavoriteProducts()
            .stream().filter(dt -> dt.getId().getProductId().compareTo(productFavoriteDto.getProductId()) == 0)
            .collect(Collectors.toList());

    if(productList.size() == 0){
      FavoriteProduct favoriteProduct = new FavoriteProduct(
        new FavoriteProductKey(userId, product.getId()),
        user,
        product
      );

      this.favoriteProductRepository.save(favoriteProduct);

      product.getFavoriteUser().add(favoriteProduct);
      Product saveProductFavorite = productRepository.save(product);
      return productMapper.ProductToProductFavoriteDto(saveProductFavorite);
    }else{
      user.getFavoriteProducts().removeAll(productList);
      this.favoriteProductRepository.deleteAll(productList);

      userRepository.save(user);
      ProductFavoriteDto productFavoriteDto1 = new ProductFavoriteDto();
      productFavoriteDto1.setProductId(productFavoriteDto.getProductId());
      return productFavoriteDto1;
    }
  }
}
