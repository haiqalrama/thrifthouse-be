package synrgy.thrifthouse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import synrgy.thrifthouse.dto.notification.NotificationDto;
import synrgy.thrifthouse.dto.user.UserChangePasswordDto;
import synrgy.thrifthouse.dto.user.UserDto;
import synrgy.thrifthouse.dto.user.UserProfileRequestDto;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.mapper.UserMapper;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.ImageService;
import synrgy.thrifthouse.service.NotificationService;
import synrgy.thrifthouse.service.UserProfileService;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ImageService imageService;

    @Autowired
    private NotificationService notificationService;

    @Override
    public UserDto updateProfile(UUID userid, UserProfileRequestDto userProfileRequestDto) {

        Optional<User> user = userRepository.findById(userid);

        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User with userId" + userid + " not found");
        }

        if(userProfileRequestDto.getProfileImg() != null){
           if(!userProfileRequestDto.getProfileImg().isEmpty()){
               Date date = new Date();
               SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
               formatter.setTimeZone(TimeZone.getTimeZone("GMT+7"));
               String dateNow = formatter.format(date);

               //Delete img
               // int getIndexDelete = user.get().getProfileImg().lastIndexOf('/');
               // String deleteImage = user.get().getProfileImg().substring(getIndexDelete - 5);
               // imageService.deleteImage(deleteImage);

               // Create New Img
               int getIndex     = userProfileRequestDto.getProfileImg().getOriginalFilename().lastIndexOf('.');
               String extension = userProfileRequestDto.getProfileImg().getOriginalFilename().substring(getIndex);
               String formatPhoto   = "user_"+ userid +"_" + dateNow + extension;
               String newNamePhoto = imageService.saveImage(userProfileRequestDto.getProfileImg(), "/users/" + formatPhoto);
               user.get().setProfileImg(newNamePhoto);
           }
        }

        // Update Only This Field
        user.get().setUsername(userProfileRequestDto.getUsername());
        user.get().setFullname(userProfileRequestDto.getFullname());
        user.get().setEmail(userProfileRequestDto.getEmail());
        user.get().setGender(Character.toUpperCase(userProfileRequestDto.getGender()));
        user.get().setPhone(userProfileRequestDto.getPhone());
        user.get().setBirth(userProfileRequestDto.getBirth());

        // Save in DB
        User userSave = userRepository.save(user.get());

        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        notificationService.createNotification(
                userSave.getId(),
                NotificationDto.builder().title("Profil telah diperbaharui").type("info").body(
                        "Profil akunmu telah berhasil diperbaharui pada tanggal "+ LocalDateTime.now()
                                .format(DateTimeFormatter.ofPattern("dd MMMM yyyy, HH:mm:ss")
                                        .withLocale(Locale.forLanguageTag("id-ID"))) +". ")
                        .image(baseUrl+"/images/info-notif.png").build()
        );

        return userMapper.userToUserDto(userSave);
    }

    @Override
    public Map<String, UUID> updatePassword(UUID userid, UserChangePasswordDto userChangePasswordDto) {

        Optional<User> user = userRepository.findById(userid);

        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User with userId" + userid + " not found");
        }

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if (!passwordEncoder.matches(userChangePasswordDto.getOldPassword(), user.get().getPassword() )) {
            throw new ResourceNotFoundException("oldPassword wrong");
        }

        if(!Objects.equals(userChangePasswordDto.getNewPassword(), userChangePasswordDto.getConfirmPassword())){
            throw new ResourceNotFoundException("newPassword not same with confirmPassword");
        }

        if(Objects.equals(userChangePasswordDto.getOldPassword(), userChangePasswordDto.getNewPassword())){
            throw new ResourceNotFoundException("value newPassword same with oldPassword");
        }

        // Save New Image
        String newPassword= passwordEncoder.encode(userChangePasswordDto.getNewPassword());
        user.get().setPassword(newPassword);
        User userChangePassword = userRepository.save(user.get());
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        notificationService.createNotification(
                userChangePassword.getId(),
                NotificationDto.builder().title("Password berhasil diubah").type("info").body(
                                "Kata sandi untuk akun kamu telah berhasil diubah dan dapat digunakan " +
                                        "untuk masuk ke website atau app Thrifthouse.")
                        .image(baseUrl+"/images/info-notif.png").build()
        );

        Map<String, UUID> mapUser = new HashMap<>();
        mapUser.put("userId", userChangePassword.getId() );
        return mapUser;
    }
}
