package synrgy.thrifthouse.service.impl;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.web.multipart.MultipartFile;
import synrgy.thrifthouse.dto.bank.BankDto;
import synrgy.thrifthouse.dto.notification.NotificationDto;
import synrgy.thrifthouse.dto.order.InsertOrderDto;
import synrgy.thrifthouse.dto.order.InsertOrderDtoNew;
import synrgy.thrifthouse.dto.order.OrdersPaymentDto;
import synrgy.thrifthouse.dto.order.StoreTransaction;
import synrgy.thrifthouse.dto.order.UpdateOrderDto;
import synrgy.thrifthouse.exception.BadRequestException;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.exception.UpdateFailedException;
import synrgy.thrifthouse.model.Address;
import synrgy.thrifthouse.model.Bank;
import synrgy.thrifthouse.model.Order;
import synrgy.thrifthouse.model.Product;
import synrgy.thrifthouse.model.Store;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.AddressRepository;
import synrgy.thrifthouse.repository.BankRepository;
import synrgy.thrifthouse.repository.OrderRepository;
import synrgy.thrifthouse.repository.ProductRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.ImageService;
import synrgy.thrifthouse.service.NotificationService;
import synrgy.thrifthouse.service.OrderService;

@NoArgsConstructor
@Service
public class OrderServiceImpl implements OrderService {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BankRepository bankRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ImageService imageService;

    @Autowired
    private NotificationService notificationService;

    @Override
    public SavedOrder insertOrder(InsertOrderDto insertOrderDto) {
        Optional<User> userOptional = this.userRepository.findById(insertOrderDto.getUserId());
        Optional<Bank> bankOptional = this.bankRepository.findById(insertOrderDto.getBankId());
        Optional<Address> addressOptional = this.addressRepository.findById(insertOrderDto.getAddressId());

        if (!userOptional.isPresent()) { throw new ResourceNotFoundException("User was not found"); }
        if (!bankOptional.isPresent()) { throw new ResourceNotFoundException("Bank was not found"); }
        if (!addressOptional.isPresent()) { throw new ResourceNotFoundException("Address was not found"); }

        List<String> productNotFoundList = new ArrayList<>();
        for (UUID productId : insertOrderDto.getProductIds()) {
            Optional<Product> productOptional = this.productRepository.findById(productId);
            if (!(productOptional.isPresent())) {
                String message = String.format("Product with ID %s was not found", productId.toString());
                productNotFoundList.add(message);
            }
        }

        if (productNotFoundList.size() > 0) { throw new ResourceNotFoundException(productNotFoundList.toString()); }


        String orderCode = "TH-" + generateThriftHouseCode();

        Order order = new Order(
            null,
            orderCode,
            insertOrderDto.getProductIds().toString(),
            insertOrderDto.getDeliveryService(),
            insertOrderDto.getShippingCost(),
            insertOrderDto.getShippingService(),
            insertOrderDto.getEstimatedTimeOfDeparture(),
            insertOrderDto.getProductsPrice(),
            addressOptional.get(),
            userOptional.get(),
            bankOptional.get()
        );
        
        Order savedOrder = this.orderRepository.save(order);
        Product product = productRepository.findById(savedOrder.getListProductIds().get(0))
                .orElseThrow(()-> new ResourceNotFoundException("Product was not found"));
        notificationService.createNotification(
                savedOrder.getUser().getId(),
                NotificationDto.builder().title(savedOrder.getStatus()).status("Belum Bayar").body(
                        "Pesanan "+ order.getOrderCode() +" telah dibuat. " +
                                "Lihat detail pemesanan untuk melanjutkan pembayaran.")
                        .type("pesanan")
                        .image(product.getPhotos().get(0)).build()
        );
        return new SavedOrder(savedOrder.getId());
    }

    @Override
    public void generateTransactionsForStores() {

        List<User> users = this.userRepository.findAll();

        // SELLERS
        List<User> sellers = users.stream().filter(u -> u.getRole().equals("ROLE_SELLER")).collect(Collectors.toList());
        sellers = users.stream().filter(u -> u.getStore() != null).collect(Collectors.toList());
        
        // SELLERS' STORE
        List<Store> stores = sellers.stream().map(s -> s.getStore()).collect(Collectors.toList());

        // BUYERS
        List<User> buyers = users.stream().filter(u -> u.getRole().equals("ROLE_USER")).collect(Collectors.toList());
        
        Random rand;
        for (User user : buyers) {
            // Define userId, addressId, bankId
            // USER
            UUID userId = user.getId();

            // ADDRESS
            UUID addressId;
            if (user.getAddresses() != null) {
                Address addressTemp = new ArrayList<>(user.getAddresses()).get(0);
                addressId = addressTemp.getId();
            } else {
                continue;
            }

            // BANK
            rand = new Random();
            List<Bank> banks = this.bankRepository.findAll();
            int bankSize = banks.size();
            UUID bankId = banks.get(rand.nextInt(bankSize)).getId();
            
            // ================================================================================================================


            rand = new Random();
            int num1 = rand.nextInt(stores.size() - 5) + 5; // 5 stores minimum

            Map<UUID, List<UUID>> storeTransactionsMap = new HashMap<>();

            List<Store> selectedStores = new ArrayList<>();
            for (int i = 0; i < num1; i++) {
                int num2 = rand.nextInt(stores.size());
                Store selectedStore = stores.get(num2);
                if (!selectedStores.contains(selectedStore)) {
                    selectedStores.add(selectedStore);
                }
            }

            

            for (Store store : selectedStores) {
                List<UUID> selectedProductIds = new ArrayList<>();

                List<Product> products = this.productRepository.findAll().stream()
                    .filter(p -> p.getStore().getId().compareTo(store.getId()) == 0)
                    .collect(Collectors.toList());

                if (products.size() > 3) {
                    num1 = rand.nextInt(products.size() - 3) + 3; // 3 products minimum
                    for (int j = 0; j < num1; j++) {
                        int num2 = rand.nextInt(products.size());
                        UUID selectedProductId = products.get(num2).getId();
                        
                        if (!selectedProductIds.contains(selectedProductId)) {
                            selectedProductIds.add(selectedProductId);
                        }
                    }
                } else if (products.size() > 0) {
                    selectedProductIds = products.stream().map(p -> p.getId()).collect(Collectors.toList());;
                } else {
                    continue;
                }

                storeTransactionsMap.put(store.getId(), selectedProductIds);
            }

            List<String> deliveryServices = Arrays.asList("jne", "tiki", "pos");
            List<String> etd = Arrays.asList("2", "3", "1", "1-2", "2-3", "3-4", "1 HARI", "2 HARI", "3 HARI");
            int etdSize = etd.size();

            List<StoreTransaction> storeTransactions = new ArrayList<>();
            for (Map.Entry<UUID, List<UUID>> transaction : storeTransactionsMap.entrySet()) {
                StoreTransaction storeTransaction = new StoreTransaction();
                storeTransaction.setProductIds(transaction.getValue());
                storeTransaction.setShippingCost((int) Math.round((1 + rand.nextInt(3) + Math.random())*1000));

                String deliveryService = deliveryServices.get(rand.nextInt(3));
                storeTransaction.setDeliveryService(deliveryService);

                switch (deliveryService) {
                    case "jne":
                        List<String> jneServices = Arrays.asList("SPS", "REG", "YES", "OKE");
                        storeTransaction.setShippingService(jneServices.get(rand.nextInt(4)));
                        break;
                    case "tiki":
                        List<String> tikiServices = Arrays.asList("ONS", "REG", "ECO");
                        storeTransaction.setShippingService(tikiServices.get(rand.nextInt(3)));
                        break;
                    case "pos":
                        List<String> posServices = Arrays.asList("Paket Kilat Khusus", "Paket Express", "Paket Jumbo Ekonomi");
                        storeTransaction.setShippingService(posServices.get(rand.nextInt(3)));
                        break;
                }
                storeTransaction.setEstimatedTimeOfDeparture(etd.get(rand.nextInt(etdSize)));

                storeTransactions.add(storeTransaction);
            }

            InsertOrderDtoNew insertOrder = new InsertOrderDtoNew();
            insertOrder.setUserId(userId);
            insertOrder.setAddressId(addressId);
            insertOrder.setBankId(bankId);
            insertOrder.setStoreTransactions(storeTransactions);

            this.insertOrderNew(insertOrder);
        }
    }

    @Override
    public Map<String, String> insertOrderNew(InsertOrderDtoNew insertOrderDto) {
        Optional<User> userOptional = this.userRepository.findById(insertOrderDto.getUserId());
        Optional<Bank> bankOptional = this.bankRepository.findById(insertOrderDto.getBankId());
        Optional<Address> addressOptional = this.addressRepository.findById(insertOrderDto.getAddressId());

        if (!userOptional.isPresent()) { throw new ResourceNotFoundException("User was not found"); }
        if (!bankOptional.isPresent()) { throw new ResourceNotFoundException("Bank was not found"); }
        if (!addressOptional.isPresent()) { throw new ResourceNotFoundException("Address was not found"); }
        if (insertOrderDto.getStoreTransactions() == null || insertOrderDto.getStoreTransactions().isEmpty()) { throw new BadRequestException("'storeTransactions' is empty! Buyer is not buying anything."); }

        User user = userOptional.get();
        Bank bank = bankOptional.get();
        Address address = addressOptional.get();

        List<Order> savedOrdersTemp = new ArrayList<>();
        for (StoreTransaction transaction : insertOrderDto.getStoreTransactions()) {
            if (transaction.getProductIds() == null || transaction.getProductIds().isEmpty()) { throw new BadRequestException("'productIds' is empty! Buyer did not add any products."); }

            String orderCode = "TH-" + generateThriftHouseCode();

            Order order = new Order();
            
            order.setUser(user);
            order.setBank(bank);
            order.setAddress(address);

            order.setOrderCode(orderCode);
            order.setDeliveryService(transaction.getDeliveryService());
            order.setShippingCost(transaction.getShippingCost());
            order.setShippingService(transaction.getShippingService());
            order.setEstimatedTimeOfDeparture(transaction.getEstimatedTimeOfDeparture());

            Integer productsPrice = 0;
            List<String> productNotFoundList = new ArrayList<>();
            for (UUID productId : transaction.getProductIds()) {
                Optional<Product> productOptional = this.productRepository.findById(productId);
                if (!(productOptional.isPresent())) {
                    productNotFoundList.add(productId.toString());
                } else {
                    Product product = productOptional.get();
                    productsPrice += product.getPrice();
                }
            }

            if (productNotFoundList.size() > 0) { throw new ResourceNotFoundException(String.format("These products were not found: %s", productNotFoundList.toString())); }

            order.setProductIds(transaction.getProductIds().toString());
            order.setProductsPrice(productsPrice);

            savedOrdersTemp.add(order);
        }

        List<Order> savedOrders = this.orderRepository.saveAll(savedOrdersTemp);
        
        for (Order savedOrder : savedOrders) {
            Product product = productRepository.findById(savedOrder.getListProductIds().get(0))
                    .orElseThrow(()-> new ResourceNotFoundException("Product was not found"));
            notificationService.createNotification(
                    savedOrder.getUser().getId(),
                    NotificationDto.builder().title(savedOrder.getStatus()).status("Belum Bayar").body(
                            "Pesanan "+ savedOrder.getOrderCode() +" telah dibuat. " +
                                    "Lihat detail pemesanan untuk melanjutkan pembayaran.")
                            .type("pesanan")
                            .image(product.getPhotos().get(0)).build()
            );
        }

        Map<String, String> response = new HashMap<>();
        int c = 0;
        for (Order o : savedOrders) {
            c++;
            response.put(String.format("order_id_%d", c), o.getId().toString());

        }
        return response;
    }

    @Override
    public OrdersPaymentDto getOrdersPayment(UUID id) {
        Optional<Order> orderOptional = this.orderRepository.findById(id);
        if (!orderOptional.isPresent()) { throw new ResourceNotFoundException("Order was not found"); }
        
        Order order = orderOptional.get();

        Bank bank = order.getBank();
        Optional<Bank> bankOptional = this.bankRepository.findById(bank.getId());
        if (!bankOptional.isPresent()) { throw new ResourceNotFoundException("Bank was not found"); }

        List<UUID> productIds = new ArrayList<>();
        String stringProductIds = order.getProductIds();
        for (String stringuuid : stringProductIds.substring(1, stringProductIds.length() - 1).split(", ")) {
            productIds.add(UUID.fromString(stringuuid));
        }

        Product product = this.productRepository.findById(productIds.get(0)).get();

        return new OrdersPaymentDto(
            order.getId(),
            order.getUser().getId(),
            product.getStore().getId(),
            order.getOrderCode(),
            new BankDto(
                bank.getId(),
                bank.getBankName(),
                bank.getBankNumber(),
                bank.getBankHolder(),
                bank.getBankLogo()
            ),
            order.getStatus(),
            order.getPackagingStatus(),
            order.getExpiredAt().toString(),
            order.getProductsPrice(),
            order.getDeliveryService(),
            order.getShippingCost(),
            order.getShippingService(),
            order.getEstimatedTimeOfDeparture(),
            order.getReceipt(),
            productIds
        );
    }

    @Override
    @Transactional
    public void updateOrderStatus(UUID id, UpdateOrderDto updateOrderDto, String status) {
        Integer rowAffected;
        if (updateOrderDto != null) {
            String receipt = updateOrderDto.getReceipt();
            rowAffected = this.orderRepository.putReceipt(id, receipt);
            if (!(rowAffected > 0)) { throw new UpdateFailedException("Failed to update receipt"); }
        } else if (status != null) {
            rowAffected = this.orderRepository.putStatus(id, status);
            if (!(rowAffected > 0)) { throw new UpdateFailedException("Failed to update status"); }
            else {
                if (status.toLowerCase(Locale.ROOT).equals("pembayaran terkonfirmasi")) {
                    String productIds = this.orderRepository.findById(id).map(o -> o.getProductIds()).orElseThrow(() -> new ResourceNotFoundException("Order Not Found"));
                    int len = productIds.length();

                    List<Product> found = this.productRepository.saveAll(productRepository.findAllById(
                            Arrays.stream(
                                productIds.substring(1, len - 1).split(", ")
                            ).map(UUID::fromString).collect(Collectors.toList())
                    ).stream().peek(product -> product.setSold(true)).collect(Collectors.toList()));
                    
                    if (found.size() <= 0) throw new UpdateFailedException("Failed to update status sold product");
                } else if (status.toLowerCase(Locale.ROOT).equals("pesanan diproses")) {
                    this.orderRepository.putPackagingStatus(id, "Menunggu dikemas");
                }
            }
        }
    }

    @Override
    public String storePaymentReceipt(MultipartFile image, UUID id) {
        boolean b = orderRepository.existsById(id);
        if (b) {
            String extension = image.getOriginalFilename();
            if (extension==null) throw new RuntimeException("Could not store file!");
            extension = extension.substring(image.getOriginalFilename().lastIndexOf("."));
            String now = LocalDateTime.now(ZoneId.of("Asia/Jakarta"))
                    .format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HHmmss"));

            String filename = "orders_" + id + "_" + now + extension;

            String s = imageService.saveImage(image, "/payment/" + filename);
            this.updateOrderStatus(id, new UpdateOrderDto(s), null);

            return s;
        } else {
            throw new ResourceNotFoundException("Order was not found");
        }
    }


    @Override
    @Transactional
    public void updateTransactionStatus(UUID transactionId, String orderStatus, String packagingStatus) {
        if (orderStatus == null && packagingStatus == null) { throw new UpdateFailedException("Missing query parameter. Declare either order_status or packaging_status."); }

        try {
            if (orderStatus != null) {
                orderStatus = orderStatus.toLowerCase();

                if (orderStatus.equals("ditolak")) {
                    this.orderRepository.putIsRejectedToTrue(transactionId);
                    this.updateOrderStatus(transactionId, null, "Pesanan ditolak");
                    this.createNotification(transactionId, "Pesanan ditolak");
                } else if (orderStatus.equals("terkonfirmasi")) {
                    this.updateOrderStatus(transactionId, null, "Pembayaran terkonfirmasi");
                    this.createNotification(transactionId, "Pembayaran terkonfirmasi");
                } else if (orderStatus.equals("diproses")) {
                    this.updateOrderStatus(transactionId, null, "Pesanan diproses");
                    this.createNotification(transactionId, "Menunggu dikemas");
                }
            } else if (packagingStatus != null) {
                packagingStatus = packagingStatus.toLowerCase();
                
                if (packagingStatus.equals("dikemas")) {
                    this.orderRepository.putPackagingStatus(transactionId, "Pesanan dikemas");
                    this.createNotification(transactionId, "Pesanan dikemas");
                } else if (packagingStatus.equals("dikirim")) {
                    this.orderRepository.putPackagingStatus(transactionId, "Pesanan dikirim");
                    this.createNotification(transactionId, "Pesanan dikirim");
                }
            }
        } catch (Exception e) {
            System.out.println("OrderServiceImpl: " + e);
            throw new UpdateFailedException("Something wrong when updating a transaction");
        }
    }

    public String generateThriftHouseCode() {
        long a = System.currentTimeMillis();
        long b = (long) (Math.ceil(Math.abs(Math.random())) * 500);
        return String.valueOf(a * b).substring(4);
    }

    private void createNotification(UUID orderId, String status) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(()-> new ResourceNotFoundException("Transaction was not found"));
        Product product = productRepository.findById(order.getListProductIds().get(0))
                .orElseThrow(() -> new ResourceNotFoundException("Product was not found"));
        NotificationDto.NotificationDtoBuilder notificationDto = NotificationDto.builder().type("pesanan")
                .image(product.getPhotos().get(0));
        switch (status.toLowerCase(Locale.ROOT)) {
            case "pesanan ditolak":
                notificationDto.title(status).status("Dibatalkan").body(
                        "Pesanan "+ order.getOrderCode() +" telah ditolak. " +
                                "Lihat detail pemesanan untuk informasi lebih lanjut.");
                break;
            case "pesanan dibatalkan":
                notificationDto.title(status).status("Dibatalkan").body(
                        "Pesanan "+ order.getOrderCode() +" telah dibatalkan. " +
                                "Lihat detail pemesanan untuk informasi lebih lanjut.");
                break;
            case "pembayaran terkonfirmasi":
                notificationDto.title(status).status("Dikemas").body(
                        "Pembayaran Pemesanan "+ order.getOrderCode() +" telah dikonfirmasi. " +
                                "Produk akan segera dikirimkan.");
                break;
        }

        switch (status.toLowerCase(Locale.ROOT)) {
            case "menunggu dikemas":
                notificationDto.title(status).status("Dikemas").body(
                        "Pesanan "+ order.getOrderCode() +" masih dalam tahap pemrosesan. " +
                                "Produk akan segera dikemas.");
                break;
            case "pesanan dikemas":
                notificationDto.title(status).status("Dikemas").body(
                        "Pesanan "+ order.getOrderCode() +" sedang dikemas. " +
                                "Produk akan segera dikirimkan.");
                break;
            case "pesanan dikirim":
                notificationDto.title(status).status("Dikirim").body(
                        "Pesanan "+ order.getOrderCode() +" telah dikirim. " +
                                "Mohon konfirmasi pemesanan jika paket telah diterima.");
                break;
        }
        notificationService.createNotification(order.getUser().getId(), notificationDto.build());
    }

}

@Data
@AllArgsConstructor
class SavedOrder {

    private UUID orderId;

    // System.out.println("\nUser's name: " + user.getUsername());
    // for (Map.Entry<UUID, List<UUID>> transaction : storeTransactionsMap.entrySet()) {
    //     System.out.println("TOKO: ");
    //     System.out.println(transaction.getKey());
        
    //     System.out.println("\nProducts: ");
    //     System.out.println(transaction.getValue());
    // }
    // System.out.println("\n");

}
