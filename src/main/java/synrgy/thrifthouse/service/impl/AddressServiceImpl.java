package synrgy.thrifthouse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import synrgy.thrifthouse.dto.address.AddressResponseDto;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.mapper.AddressMapper;
import synrgy.thrifthouse.model.Address;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.AddressRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.AddressService;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AddressServiceImpl implements AddressService {

    public AddressRepository addressRepository;
    public UserRepository userRepository;
    public AddressMapper addressMapper;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository, UserRepository userRepository, AddressMapper addressMapper){
        this.addressRepository = addressRepository;
        this.userRepository    = userRepository;
        this.addressMapper = addressMapper;
    }

    
    @Override
    public List<AddressResponseDto> getAll() {
        List<Address> addressList = addressRepository.findAll();
        return addressList.stream().map(this::convertAddressToAddressResponseDto).collect(Collectors.toList());
    }

    @Override
    public List<AddressResponseDto> getAllByUserId(UUID userId) {
        // Get All Data address by User Id
        final List<Address> addressList = addressRepository.getAllByUserId(userId);
        if(!(addressList.size() == 0)){
            // Convert Address to AddressResponseDto
            return addressList.stream()
                    .map(this::convertAddressToAddressResponseDto)
                    .collect(Collectors.toList());
        }else{
            throw new ResourceNotFoundException("Address with userId " + userId + " not found");
        }
    }

    @Override
    public AddressResponseDto getByAddressId(UUID userId, UUID addressId) {
        Optional<User> user = userRepository.findById(userId);
        if(!user.isPresent()){
            throw new ResourceNotFoundException("Address with userId " + userId + " not found");
        }

        Optional<Address> address = addressRepository.findById(addressId);
        if(!address.isPresent()){
            throw new ResourceNotFoundException("AddressId " + addressId + " not found");
        }

        return addressMapper.AddressToAddressResponseDto(address.get());
    }


    @Override
    public AddressResponseDto store(UUID userId, AddressResponseDto addressResponseDto) {
        return this.saveOrUpdate(userId, null, addressResponseDto);
    }

    @Override
    public AddressResponseDto updateAddress(UUID userId, UUID addressId, AddressResponseDto addressResponseDto) {
        Optional<Address> address = addressRepository.findById(addressId);
        if(!address.isPresent()){
            throw new ResourceNotFoundException("AddressId " + addressId + " not found");
        }
        return this.saveOrUpdate(userId, addressId, addressResponseDto);
    }

    private AddressResponseDto saveOrUpdate(UUID userId, UUID addressId, AddressResponseDto addressResponseDto){
        // Find User By Id
        final Optional<User> user = userRepository.findById(userId);
        if(user.isPresent()){
            // Check When Checkbox False
            if (addressResponseDto.getAddressLabel() == null) addressResponseDto.setAddressLabel(false);
            if(!addressResponseDto.getAddressLabel()){
                Address addressStore = convertToAddress(user.get(), addressResponseDto);
                addressStore.setId(addressId);
                Address address      = addressRepository.save(addressStore);
                // Convert Address To AddressResponseDto
                return this.convertAddressToAddressResponseDto(address);
            }else{
                final List<Address> addressList = addressRepository.getAllByUserId(userId);
                List<Address> newAddressList    = addressList.stream()
                        .map(dt -> convertToAddress(user.get(), dt  , false)).collect(Collectors.toList());
                Address newAddress = convertToAddress(user.get(), addressResponseDto);
                newAddress.setId(addressId);
                newAddressList.add(newAddress);
                List<Address> saveAddressList      = addressRepository.saveAll(newAddressList);
                // Convert Address To AddressResponseDto
                List<Address> fillterAddressTrue = saveAddressList.stream().filter(Address::getAddressLabel).collect(Collectors.toList());
                return this.convertAddressToAddressResponseDto(fillterAddressTrue.get(0));
            }
        }else {
            throw new ResourceNotFoundException("Address with userId " + userId + " not found");
        }
    }

    @Override
    public Map<String, UUID> deleteByAddressId(UUID userId, UUID addressId) {

        Optional<User> user = userRepository.findById(userId);
        if(!user.isPresent()){
            throw new ResourceNotFoundException("Address with userId " + userId + " not found");
        }

        Optional<Address> address = addressRepository.findById(addressId);
        if(!address.isPresent()){
            throw new ResourceNotFoundException("AddressId " + addressId + " not found");
        }

        addressRepository.deleteById(address.get().getId());

        Map<String, UUID > response = new HashMap<>();
        response.put("addressId", addressId);
        return response;
    }

    public Address convertToAddress(User user, AddressResponseDto addressResponseDto){
        Address address = new Address();
        address.setUser(user);
        address.setRecipientName(addressResponseDto.getRecipientName());
        address.setRecipientPhone(addressResponseDto.getRecipientPhone());
        address.setAddressLabel(addressResponseDto.getAddressLabel());
        address.setIdProvince(addressResponseDto.getIdProvince());
        address.setProvince(addressResponseDto.getProvince());
        address.setIdCity(addressResponseDto.getIdCity());
        address.setCity(addressResponseDto.getCity());
        address.setDistrict(addressResponseDto.getDistrict());
        address.setVillage(addressResponseDto.getVillage());
        address.setFullAddress(addressResponseDto.getFullAddress());
        address.setPostalCode(addressResponseDto.getPostalCode());
        address.setDetail(addressResponseDto.getDetail());
        return address;
    }

    public Address convertToAddress( User user, Address address, Boolean addressLabel){
        Address newAddress = new Address();
        newAddress.setUser(user);
        newAddress.setId(address.getId());
        newAddress.setRecipientName(address.getRecipientName());
        newAddress.setRecipientPhone(address.getRecipientPhone());
        newAddress.setAddressLabel(addressLabel);
        newAddress.setIdProvince(address.getIdProvince());
        newAddress.setProvince(address.getProvince());
        newAddress.setIdCity(address.getIdCity());
        newAddress.setCity(address.getCity());
        newAddress.setDistrict(address.getDistrict());
        newAddress.setVillage(address.getVillage());
        newAddress.setFullAddress(address.getFullAddress());
        newAddress.setPostalCode(address.getPostalCode());
        newAddress.setDetail(address.getDetail());
        return newAddress;
    }

    public AddressResponseDto convertAddressToAddressResponseDto(Address address){
        AddressResponseDto addressResponseDto = new AddressResponseDto();
        addressResponseDto.setId(address.getId());
        addressResponseDto.setRecipientName(address.getRecipientName());
        addressResponseDto.setRecipientPhone(address.getRecipientPhone());
        addressResponseDto.setAddressLabel(address.getAddressLabel());
        addressResponseDto.setIdProvince(address.getIdProvince());
        addressResponseDto.setProvince(address.getProvince());
        addressResponseDto.setIdCity(address.getIdCity());
        addressResponseDto.setCity(address.getCity());
        addressResponseDto.setDistrict(address.getDistrict());
        addressResponseDto.setVillage(address.getVillage());
        addressResponseDto.setFullAddress(address.getFullAddress());
        addressResponseDto.setPostalCode(address.getPostalCode());
        addressResponseDto.setDetail(address.getDetail());
        return addressResponseDto;
    }
}
