package synrgy.thrifthouse.service.impl;

import org.springframework.stereotype.Service;
import synrgy.thrifthouse.dto.notification.NotificationDto;
import synrgy.thrifthouse.dto.product.ProductWithStoreDto;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.exception.UpdateFailedException;
import synrgy.thrifthouse.mapper.AddressMapper;
import synrgy.thrifthouse.mapper.ProductMapper;
import synrgy.thrifthouse.model.Order;
import synrgy.thrifthouse.model.Product;
import synrgy.thrifthouse.model.Store;
import synrgy.thrifthouse.repository.OrderRepository;
import synrgy.thrifthouse.repository.ProductRepository;
import synrgy.thrifthouse.repository.StoreRepository;
import synrgy.thrifthouse.service.NotificationService;
import synrgy.thrifthouse.service.OrderUserService;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class OrderUserServiceImpl implements OrderUserService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final NotificationService notificationService;
    private final StoreRepository storeRepository;
    private final AddressMapper addressMapper;

    public OrderUserServiceImpl(OrderRepository orderRepository, ProductRepository productRepository, ProductMapper productMapper, NotificationService notificationService, StoreRepository storeRepository, AddressMapper addressMapper) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.notificationService = notificationService;
        this.storeRepository = storeRepository;
        this.addressMapper = addressMapper;
    }

    @Override
    public List<Map<String, Object>> getAll(UUID userId, String status) {
        List<Order> orders;
        if (status!=null){
            switch (status) {
                case "bayar":
//                statusSend = "Menunggu pembayaran";
                    orders = orderRepository.findByUser_IdAndStatusAllIgnoreCaseOrderByExpiredAtDesc(userId, "Menunggu pembayaran");
                    break;
                case "dikemas":
//                packagingStatus = "Pesanan Dikemas";
                    orders = orderRepository.findByUser_IdAndPackagingStatusInIgnoreCaseAllIgnoreCaseOrderByExpiredAtDesc(userId, Arrays.asList("Pesanan Dikemas", "Menunggu dikemas"));
                    break;
                case "dikirim":
//                packagingStatus = "Pesanan Dikirim";
                    orders = orderRepository.findByUser_IdAndPackagingStatusAllIgnoreCaseOrderByExpiredAtDesc(userId, "Pesanan Dikirim");
                    break;
                case "selesai":
//                packagingStatus = "Pesanan Diterima";
                    orders = orderRepository.findByUser_IdAndPackagingStatusAllIgnoreCaseOrderByExpiredAtDesc(userId, "Pesanan Diterima");
                    break;
                case "dibatalkan":
//                isRejected = true;
                    orders = orderRepository.findByUser_IdAndIsRejectedOrderByExpiredAtDesc(userId, true);
                    break;
                default:
                    orders = orderRepository.findByUser_IdOrderByExpiredAtDesc(userId);
                    break;
            }
        } else {
            orders = orderRepository.findByUser_IdOrderByExpiredAtDesc(userId);
        }
        return orders.stream().map(order -> {
            List<UUID> listProductIds = order.getListProductIds();
            List<Product> productList = productRepository.findAllById(listProductIds);
            List<Map<String, Object>> stores = new ArrayList<>();
            Map<String, Object> result = new HashMap<>();
            Map<UUID, List<ProductWithStoreDto>> collect = productList.stream()
                    .map(productMapper::ProductToProductWithStoreDto)
                    .collect(Collectors.groupingBy(product -> product.getStore().getId()));
            collect.forEach(((uuid, productWithStoreDtos) -> {
                Map<String, Object> store = new HashMap<>();
                store.put("id", uuid);
                store.put("name", productWithStoreDtos.get(0).getStore().getName());
                store.put("province", productWithStoreDtos.get(0).getStore().getProvince());
                store.put("province_id", productWithStoreDtos.get(0).getStore().getProvince_id());
                store.put("city", productWithStoreDtos.get(0).getStore().getCity());
                store.put("city_id", productWithStoreDtos.get(0).getStore().getCity_id());
                store.put("photo", this.storeRepository.findById(uuid).get().getLogo());
                Stream<ProductWithStoreDto> peek = productWithStoreDtos.stream().peek(productWithStoreDto -> productWithStoreDto.setStore(null));
                store.put("products", peek);
                stores.add(store);
            }));
            result.put("id", order.getId());
            result.put("status", order.getStatus());
            result.put("packagingStatus", order.getPackagingStatus());
            result.put("orderCode", order.getOrderCode());
            result.put("store", stores);
            return result;
        }).collect(Collectors.toList());
    }


    @Override
    public void acceptOrder(UUID orderId) {
        Optional<Order> orderOptional = this.orderRepository.findById(orderId);
        if (!orderOptional.isPresent()) { throw new ResourceNotFoundException("Transaction was not found"); }
        Integer updateStatus = this.orderRepository.putStatus(orderId, "Selesai");
        Integer updatePackagingStatus = this.orderRepository.putPackagingStatus(orderId, "Pesanan diterima");

        if (!(updateStatus > 0) || !(updatePackagingStatus > 0)) { throw new UpdateFailedException("Failed updating transaction"); }

        Product product = productRepository.findById(orderOptional.get().getListProductIds().get(0))
                .orElseThrow(() -> new ResourceNotFoundException("Product was not found"));
        notificationService.createNotification(
                orderOptional.get().getUser().getId(),
                NotificationDto.builder().title("Pesanan telah diterima").type("pesanan").status("Selesai").body(
                                "Pesanan " + orderOptional.get().getOrderCode() +" telah Anda terima pada " +
                                        LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd MMMM yyyy, HH:mm:ss")
                                                        .withLocale(Locale.forLanguageTag("id-ID")))+ ". " +
                                        "Lihat detail pemesanan untuk memberikan ulasan pada toko.")
                        .image(product.getPhotos().get(0)).build()
        );
    }

    @Override
    public Map<String, Object> getOrderDetailById(UUID userId, UUID orderId) {
        Order order = orderRepository.findByIdAndUser_Id(orderId,userId)
                .orElseThrow(()->new ResourceNotFoundException("Order or User was not found"));
        List<UUID> listProductIds = order.getListProductIds();
        List<Product> productList = productRepository.findAllById(listProductIds);
        List<Map<String, Object>> stores = new ArrayList<>();
        Map<String, Object> result = new HashMap<>();
        Map<UUID, List<ProductWithStoreDto>> collect = productList.stream()
                .map(productMapper::ProductToProductWithStoreDto)
                .collect(Collectors.groupingBy(product -> product.getStore().getId()));
        collect.forEach(((uuid, productWithStoreDtos) -> {
            Map<String, Object> store = new HashMap<>();
            store.put("id", uuid);
            store.put("name", productWithStoreDtos.get(0).getStore().getName());
            store.put("province", productWithStoreDtos.get(0).getStore().getProvince());
            store.put("province_id", productWithStoreDtos.get(0).getStore().getProvince_id());
            store.put("city", productWithStoreDtos.get(0).getStore().getCity());
            store.put("city_id", productWithStoreDtos.get(0).getStore().getCity_id());
            store.put("photo", this.storeRepository.findById(uuid).get().getLogo());
            Stream<ProductWithStoreDto> peek = productWithStoreDtos.stream().peek(productWithStoreDto -> productWithStoreDto.setStore(null));
            store.put("products", peek);
            Store store1 = storeRepository.findById(productWithStoreDtos.get(0).getStore().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Product was not found"));
            store.put("deliveryService",
                    store1.getDeliveryService()
            );
            store.put("estimatedTimeOfDeparture",
                    order.getEstimatedTimeOfDeparture()
            );
            store.put("shippingService", order.getShippingService());
            stores.add(store);
        }));
        result.put("id", order.getId());
        result.put("status", order.getStatus());
        result.put("packagingStatus", order.getPackagingStatus());
        result.put("orderCode", order.getOrderCode());
        result.put("shippingCost", order.getShippingCost());
        result.put("productsPrice", order.getProductsPrice());
        result.put("address", addressMapper.AddressToAddressResponseDto(order.getAddress()));
        result.put("store", stores);
        return result;
    }
}
