package synrgy.thrifthouse.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import synrgy.thrifthouse.dto.dashboard.DashboardSummaryDto;
import synrgy.thrifthouse.dto.dashboard.OrderedProductDetailDto;
import synrgy.thrifthouse.dto.dashboard.PaginationTransactionDto;
import synrgy.thrifthouse.dto.dashboard.StoreSummaryDto;
import synrgy.thrifthouse.dto.dashboard.TransactionDetailDto;
import synrgy.thrifthouse.dto.dashboard.TransactionDto;
import synrgy.thrifthouse.dto.notification.NotificationDto;
import synrgy.thrifthouse.dto.order.InsertOrderDto;
import synrgy.thrifthouse.exception.BadRequestException;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.exception.UserCredentialException;
import synrgy.thrifthouse.mapper.StoreMapper;
import synrgy.thrifthouse.model.Address;
import synrgy.thrifthouse.model.Bank;
import synrgy.thrifthouse.model.Order;
import synrgy.thrifthouse.model.Product;
import synrgy.thrifthouse.model.Store;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.*;
import synrgy.thrifthouse.service.DashboardService;
import synrgy.thrifthouse.service.NotificationService;

@Service
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private BankRepository bankRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StoreMapper storeMapper;

    @Autowired
    private NotificationService notificationService;


    @Override
    public DashboardSummaryDto getDashboardSummary(UUID userId, Integer page, Integer size, String status, String sortOrder, String id_transaksi) {
        Optional<User> userOptional = this.userRepository.findById(userId);
        if (!userOptional.isPresent()) { throw new ResourceNotFoundException("User (Seller) was not found"); }
        UUID storeId = userOptional.get().getStore().getId();


        StoreSummaryDto storesSummary = this.storeRepository.findById(storeId).map(storeMapper::StoreToStoreSummaryDto).orElseThrow(() -> new ResourceNotFoundException("Store was not found"));

        Map<UUID, List<UUID>> allStoreTransactions = new HashMap<>();
        
        for (Order transaction : this.orderRepository.findAll()) {
            List<UUID> orderedProducts = new ArrayList<>();
            String productIds = transaction.getProductIds();

            Boolean isFound = false;
            for (String productId : productIds.substring(1, productIds.length() - 1).split(", ")) {
                if (productId.length() > 0) {
                    Optional<Product> productOptional = this.productRepository.findById(UUID.fromString(productId));
                    if (!productOptional.isPresent()) { throw new ResourceNotFoundException(String.format("Product %s was not found", productId)); }
    
                    Product product = productOptional.get();
                    if (product.getStore().getId().compareTo(storeId) == 0) {
                        orderedProducts.add(product.getId());
                        isFound = true;
                    }
                }
            }

            if (isFound) {
                allStoreTransactions.put(transaction.getId(), orderedProducts);
            }
        }

        List<TransactionDto> transactionDtos = new ArrayList<>();
        for (Map.Entry<UUID, List<UUID>> transaction : allStoreTransactions.entrySet()) {
            Order order = this.orderRepository.findById(transaction.getKey()).get();

            checkExpiredTransactions(order);

            Integer totalPrice = 0;
            String buyerName = order.getUser().getUsername();
            
            for (Product product : this.productRepository.findAllById(transaction.getValue())) {
                totalPrice += product.getPrice();
            }

            // Subtract expired date by 1 day, and
            // convert to Indonesian date formatting
            Date orderedAt_Date = new Date((order.getExpiredAt().getTime() - 86400000L) + 7 * (3600000)); // damn it UTC!

            SimpleDateFormat formatter = new SimpleDateFormat("dd MMMMMMMM yyyy", new Locale("id", "ID"));
            String orderedAt = formatter.format(orderedAt_Date);

            transactionDtos.add(new TransactionDto(
                transaction.getKey(),
                order.getOrderCode(),
                totalPrice,
                buyerName,
                orderedAt,
                orderedAt_Date,
                order.getStatus(),
                order.getPackagingStatus(),
                order.getReceipt()
            ));
        }

        Integer revenue = 0;
        Integer grossIncome = 0;
        for (TransactionDto order : transactionDtos) {
            revenue += order.getPrice();
            grossIncome = grossIncome + (int) Math.ceil(1.06 * (double) order.getPrice()); 
        }

        storesSummary.setRevenue(revenue);
        storesSummary.setTransaction(grossIncome);

        try {
            
            if (id_transaksi != null) {
                transactionDtos = transactionDtos.stream()
                    .filter(order -> order.getKodeTransaksi().equals(id_transaksi))
                    .collect(Collectors.toList());
            } else {
                switch (status) {
                    case "semua":
                        Comparator<TransactionDto> compareReceipt = Comparator.comparing(TransactionDto::getReceiptIsPut);
                        switch (sortOrder) {
                            case "dibuat_terbaru":
                                transactionDtos = transactionDtos.stream()
                                    .sorted((order1, order2) -> {
                                        Integer time1 = (int) order1.getOrderedAt_DateType().getTime();
                                        Integer time2 = (int) order2.getOrderedAt_DateType().getTime();
                                        return -1 * time1.compareTo(time2);
                                    })
                                    .sorted(compareReceipt)
                                    .collect(Collectors.toList());
                                break;
                            case "dibuat_terlama":
                                transactionDtos = transactionDtos.stream()
                                    .sorted((order1, order2) -> {
                                        Integer time1 = (int) order1.getOrderedAt_DateType().getTime();
                                        Integer time2 = (int) order2.getOrderedAt_DateType().getTime();
                                        return time1.compareTo(time2);
                                    })
                                    .sorted(compareReceipt)
                                    .collect(Collectors.toList());
                                break;
                            case "dibayar_terbaru":
                                transactionDtos = transactionDtos.stream()
                                    .sorted(compareReceipt)
                                    .sorted((order1, order2) -> {
                                        Integer time1 = (int) order1.getOrderedAt_DateType().getTime();
                                        Integer time2 = (int) order2.getOrderedAt_DateType().getTime();
                                        return -1 * time1.compareTo(time2);
                                    })
                                    .collect(Collectors.toList());
                                break;
                            case "dibayar_terlama":
                                transactionDtos = transactionDtos.stream()
                                    .sorted((order1, order2) -> {
                                        Integer time1 = (int) order1.getOrderedAt_DateType().getTime();
                                        Integer time2 = (int) order2.getOrderedAt_DateType().getTime();
                                        return time1.compareTo(time2);
                                    })
                                    .sorted(Collections.reverseOrder(compareReceipt))
                                    .collect(Collectors.toList());
                                break;
                        }
                        break;
                    case "dibatalkan":
                        transactionDtos = transactionDtos.stream()
                            .filter(order -> {
                                String orderStatus = order.getStatus().toLowerCase();
                                return orderStatus.contains("ditolak") || orderStatus.contains("dibatalkan");
                            })
                            .collect(Collectors.toList());
                        break;
                    case "selesai":
                        transactionDtos = transactionDtos.stream()
                            .filter(order -> {
                                String orderStatus = order.getStatus().toLowerCase();
                                return orderStatus.contains("selesai");
                            })
                            .collect(Collectors.toList());
                        break;
                    default:
                        transactionDtos = transactionDtos.stream()
                            .filter(order -> order.getPackagingStatus().toLowerCase().contains(status))
                            .collect(Collectors.toList());
                    
                
                }
            }
        } catch (Exception e) {
            System.out.println("DashboardServiceImpl: " + e);
            throw e;
        }
        

        // === PaginationTransactionDto ===
        Long length = (long) (transactionDtos.size());
        Integer totalPages = null;

        PaginationTransactionDto transactions = new PaginationTransactionDto();
        transactions.setTotalItems(length);
        transactions.setCurrentPage(page);

        if (size != 0) {
            totalPages = (int) Math.ceil(length / (double) size);
    
            int a = page * size;
            int b = size * (page+1);
            
            transactionDtos = transactionDtos.subList(
                a > length ? (int) (long) length : a,
                b > length ? (int) (long) length : b
            );

            transactions.setTotalPages(totalPages);
            transactions.setTransactions(transactionDtos);
        } else {
            totalPages = (Integer.valueOf((int) (long) length) / 1);

            transactions.setTotalPages(totalPages);
            transactions.setTransactions(transactionDtos.subList(1, 1));
        }

        return new DashboardSummaryDto(storeId, storesSummary, transactions);
    }

    @Override
    public TransactionDetailDto getTransactionDetail(UUID orderId) {

        Optional<Order> orderOptional = this.orderRepository.findById(orderId);
        if (!orderOptional.isPresent()) { throw new ResourceNotFoundException("Transaction was not found"); }
        Order order = orderOptional.get();

        TransactionDetailDto transactionDetail = this.orderRepository.getTransactionDetail(orderId);

        if (order.getProductIds() == null || order.getProductIds().isEmpty()) { throw new BadRequestException("This order has no productIds! Did you correctly add the productIds when checking out?"); }
        
        List<OrderedProductDetailDto> orderedProducts = new ArrayList<>();
        String productIds = order.getProductIds();
        for (String productId : productIds.substring(1, productIds.length() - 1).split(", ")) {
            Optional<Product> productOptional = this.productRepository.findById(UUID.fromString(productId));
            if (!productOptional.isPresent()) { throw new ResourceNotFoundException(String.format("Product %s was not found", productId)); }

            Product product = productOptional.get();
            OrderedProductDetailDto productDetail = new OrderedProductDetailDto(product.getId(), product.getName(), product.getPrice());
            orderedProducts.add(productDetail);
        }

        int totalPrice = 0;

        totalPrice += order.getProductsPrice();
        totalPrice += order.getShippingCost();

        transactionDetail.setTotalPrice(totalPrice);
        transactionDetail.setProducts(orderedProducts);

        return transactionDetail;
    }

    public void deleteTransaction(UUID orderId) {
        Optional<Order> orderOptional = this.orderRepository.findById(orderId);
        if (!(orderOptional.isPresent())) { throw new ResourceNotFoundException("Transaction was not found"); }
        this.orderRepository.deleteById(orderId);
    }

    public void checkExpiredTransactions(Order order) {
        Date now = new Date();
        if (now.getTime() > order.getExpiredAt().getTime()) {
            this.orderRepository.putIsRejectedToTrue(order.getId());
            this.orderRepository.putStatus(order.getId(), "Pesanan dibatalkan");

            Product product = productRepository.findById(order.getListProductIds().get(0))
                    .orElseThrow(() -> new ResourceNotFoundException("Product was not found"));
            notificationService.createNotification(
                    order.getUser().getId(),
                    NotificationDto.builder().title(order.getStatus()).type("pesanan").status("Dibatalkan").body(
                            "Pembayaran Pemesanan "+ order.getOrderCode() +" telah dibatalkan. " +
                                    "Lihat detail pemesanan untuk informasi lebih lanjut.")
                            .image(product.getPhotos().get(0)).build()
            );
        }
    }

    public void generateTransactionsForOneStore(UUID userId) {
        // Get seller's store ID
        Optional<User> userOptional = this.userRepository.findById(userId);
        if (!userOptional.isPresent()) { throw new ResourceNotFoundException("User was not found"); }
        User seller = userOptional.get();
        if (!seller.getRole().equals("ROLE_SELLER")) { throw new UserCredentialException("User is not a seller"); }
        Store store = seller.getStore();
        UUID storeId = store.getId();

        Bank bank = this.bankRepository.findAll().get(0);

        System.out.println("\n========================================================================================================================");
        System.out.println(String.format("Toko: (store_id) %s, (user_id) %s", storeId.toString(), userId.toString()));
        System.out.println("Amount of products: " + store.getProducts().size());

        List<UUID> selectedUsers = new ArrayList<>();
        List<String> selectedNames = new ArrayList<>();
        List<UUID> selectedAddress = new ArrayList<>();

        System.out.println("\nUsers:");
        List<User> users = this.userRepository.findAll();
        int c = 0;
        for (User user: users) {
            if (c < 3) {
                if (user.getRole().equals("ROLE_USER")) {
                    String name = user.getUsername();
                    UUID id = user.getId();

                    Address address = this.addressRepository.findAll().stream()
                        .filter(adr -> adr.getUser().getId().compareTo(user.getId()) == 0)
                        .collect(Collectors.toList()).get(0);
                        
                    UUID addressId = address.getId();

                    selectedUsers.add(id);
                    selectedAddress.add(addressId);
                    selectedNames.add(name);

                    System.out.println(String.format("- %s (user_id: %s ) (address_id: %s )", name, id.toString(), addressId.toString()));
                    c++;
                }
            } else {
                break;
            }
        }

        List<Product> productsTemp = new ArrayList<>(store.getProducts());
        List<UUID> products = productsTemp.stream().map(p -> p.getId()).collect(Collectors.toList());

        List<UUID> products1 = products.subList(0, 3);
        List<UUID> products2 = products.subList(3, 6);
        List<UUID> products3 = products.subList(6, 8);

        InsertOrderDto order1 = new InsertOrderDto(selectedUsers.get(0), selectedAddress.get(0), products1, "jne", 100, "OKE", "1-2", 200, bank.getId());
        InsertOrderDto order2 = new InsertOrderDto(selectedUsers.get(1), selectedAddress.get(1), products2, "pos", 200, "YES", "2-3", 300, bank.getId());
        InsertOrderDto order3 = new InsertOrderDto(selectedUsers.get(2), selectedAddress.get(2), products3, "tiki", 300, "REG", "3-4", 400, bank.getId());

        UUID orderId1 = insertOrder(order1);
        UUID orderId2 = insertOrder(order2);
        UUID orderId3 = insertOrder(order3);

        System.out.println("\nProducts:");
        System.out.println(String.format("- %s (orderId: %s )", selectedNames.get(0), orderId1.toString()));
        for (UUID pId : products1) {
            System.out.println(String.format(" > %s", pId.toString()));
        }
        System.out.println(String.format("\n- %s (orderId: %s )", selectedNames.get(1), orderId2.toString()));
        for (UUID pId : products2) {
            System.out.println(String.format(" > %s", pId.toString()));
        }
        System.out.println(String.format("\n- %s (orderId: %s )", selectedNames.get(2), orderId3.toString()));
        for (UUID pId : products3) {
            System.out.println(String.format(" > %s", pId.toString()));
        }

        System.out.println("\n" + bank.getBankName() + ": " + bank.getId().toString());
        System.out.println("========================================================================================================================\n");        
    }

    public UUID insertOrder(InsertOrderDto insertOrderDto) {
        Optional<User> userOptional = this.userRepository.findById(insertOrderDto.getUserId());
        Optional<Bank> bankOptional = this.bankRepository.findById(insertOrderDto.getBankId());
        Optional<Address> addressOptional = this.addressRepository.findById(insertOrderDto.getAddressId());

        if (!userOptional.isPresent()) { throw new ResourceNotFoundException("User was not found"); }
        if (!bankOptional.isPresent()) { throw new ResourceNotFoundException("Bank was not found"); }
        if (!addressOptional.isPresent()) { throw new ResourceNotFoundException("Address was not found"); }

        List<String> productNotFoundList = new ArrayList<>();
        for (UUID productId : insertOrderDto.getProductIds()) {
            Optional<Product> productOptional = this.productRepository.findById(productId);
            if (!(productOptional.isPresent())) {
                String message = String.format("Product with ID %s was not found", productId.toString());
                productNotFoundList.add(message);
            }
        }

        if (productNotFoundList.size() > 0) { throw new ResourceNotFoundException(productNotFoundList.toString()); }


        String orderCode = "TH-" + System.currentTimeMillis();

        Order order = new Order(
            null,
            orderCode,
            insertOrderDto.getProductIds().toString(),
            insertOrderDto.getDeliveryService(),
            insertOrderDto.getShippingCost(),
            insertOrderDto.getShippingService(),
            insertOrderDto.getEstimatedTimeOfDeparture(),
            insertOrderDto.getProductsPrice(),
            addressOptional.get(),
            userOptional.get(),
            bankOptional.get()
        );
        
        Order savedOrder = this.orderRepository.save(order);
        return savedOrder.getId();
    }

}
