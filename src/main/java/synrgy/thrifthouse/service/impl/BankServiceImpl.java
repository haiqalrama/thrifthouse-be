package synrgy.thrifthouse.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import synrgy.thrifthouse.dto.bank.BankDto;
import synrgy.thrifthouse.dto.bank.InsertBankDto;
import synrgy.thrifthouse.mapper.BankMapper;
import synrgy.thrifthouse.repository.BankRepository;
import synrgy.thrifthouse.service.BankService;

@Service
public class BankServiceImpl implements BankService {
    
    @Autowired
    private BankRepository bankRepository;

    @Autowired
    private BankMapper bankMapper;

    @Override
    public List<BankDto> getBanks() {
        return this.bankRepository.findAll().stream().map(bankMapper::BankToBankDto).collect(Collectors.toList());
    }

    @Override
    public void insertBank(InsertBankDto bankDto) {
        this.bankRepository.save(bankMapper.BankDtoToBank(bankDto));
    }
    
}
