package synrgy.thrifthouse.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import synrgy.thrifthouse.dto.product.ProductWithStoreDto;
import synrgy.thrifthouse.dto.store.StoreSimpleDto;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.mapper.ProductMapper;
import synrgy.thrifthouse.model.Product;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.ProductRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.CartService;

@Service
public class CartServiceImpl implements CartService {

  private final UserRepository userRepository;
  private final ProductRepository productRepository;
  private final ProductMapper productMapper;

  @Autowired
  public CartServiceImpl(UserRepository userRepository, ProductRepository productRepository,
      ProductMapper productMapper) {
    this.userRepository = userRepository;
    this.productRepository = productRepository;
    this.productMapper = productMapper;
  }

  @Override
  public void addToCart(UUID userId, UUID productId) {
    Optional<User> userOptional = userRepository.findById(userId);
    userOptional.orElseThrow(() -> new ResourceNotFoundException("User not found"));
    userOptional.ifPresent(user -> {
      Optional<Product> productOptional = productRepository.findById(productId);
      productOptional.orElseThrow(() -> new ResourceNotFoundException("Product not found"));
      productOptional.ifPresent(product -> {
        user.getCart().add(product);
        userRepository.save(user);
      });
    });
  }

  @Override
  public void removeFromCart(UUID userId, UUID productId) {
    Optional<User> userOptional = userRepository.findById(userId);
    userOptional.orElseThrow(() -> new ResourceNotFoundException("User not found"));
    userOptional.ifPresent(user -> {
      if (user.getCart().stream().noneMatch(product -> product.getId().equals(productId))) {
        throw new ResourceNotFoundException("Product not found");
      }
      Optional<Product> productOptional = productRepository.findById(productId);
      productOptional.orElseThrow(() -> new ResourceNotFoundException("Product not found"));
      productOptional.ifPresent(product -> {
        user.getCart().remove(product);
        userRepository.save(user);
      });
    });
  }

  @Override
  public Collection<ProductWithStoreDto> getCart(UUID userId) throws IOException {
    Optional<User> userOptional = userRepository.findById(userId);
    
    // return userOptional.map(user -> user.getCart().stream()
    //     .map(productMapper::ProductToProductWithStoreDto)
    //     .collect(Collectors.toSet())).orElseThrow(() -> new ResourceNotFoundException("User not found"));

    return userOptional.map(user -> user.getCart().stream()
      .map(product -> {
        ProductWithStoreDto dto = productMapper.ProductToProductWithStoreDto(product);
        try {
          setCityAndProvinceIDs(dto.getStore());
        } catch (JsonProcessingException e) {  
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
        return dto;
      }).collect(Collectors.toSet())
    ).orElseThrow(() -> new ResourceNotFoundException("User not found"));

  }

  @Override
  public void bulkAddToCart(UUID userId, Set<UUID> productIds) {
    Optional<User> userOptional = userRepository.findById(userId);
    userOptional.orElseThrow(() -> new ResourceNotFoundException("User not found"));
    userOptional.ifPresent(user -> {
      List<Product> productList = productRepository.findAllById(productIds);
      if (productList.isEmpty()) throw new ResourceNotFoundException("All Product Ids not found");
      productList.forEach(product -> {
        user.getCart().add(product);
      });
      userRepository.save(user);
    });
  }

  @Override
  public void bulkRemoveFromCart(UUID userId, Set<UUID> productIds) {
    Optional<User> userOptional = userRepository.findById(userId);
    userOptional.orElseThrow(() -> new ResourceNotFoundException("User not found"));
    userOptional.ifPresent(user -> {
      List<Product> productList = productRepository.findAllById(productIds);
      if (productList.isEmpty()) throw new ResourceNotFoundException("All Product Ids not found");
      user.getCart().removeAll(productList);
      userRepository.save(user);
    });
  }

  public void setCityAndProvinceIDs(StoreSimpleDto storeDto) throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();

    Path name = Paths.get("src/main/resources/get-city-response.json");
    JsonNode cityResponse = objectMapper.readTree(
      new String(Files.readAllBytes(Paths.get(name.toUri()))).replaceAll("[\t\r\n]", "")
    );

    String cityId = "";
    String provinceId = "";
    boolean isFound = false;
    for (JsonNode result : cityResponse.path("rajaongkir").path("results")) {
      // store the city and province name
      String cityName = result.path("city_name").toString().split("\"")[1];
      String provinceName = result.path("province").toString().split("\"")[1];
      if (cityName.equals(storeDto.getCity()) && provinceName.equals(storeDto.getProvince())) {
        provinceId = result.path("province_id").toString().split("\"")[1];
        cityId = result.path("city_id").toString().split("\"")[1];
        isFound = true;
        break;
      }
    }
    if (!isFound) {
      storeDto.setProvince_id("INVALID_NAME");
      storeDto.setCity_id("INVALID_NAME");
    } else {
      storeDto.setCity_id(cityId);
      storeDto.setProvince_id(provinceId);
    }
  }


}