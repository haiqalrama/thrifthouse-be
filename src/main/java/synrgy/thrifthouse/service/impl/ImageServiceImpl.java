package synrgy.thrifthouse.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import synrgy.thrifthouse.service.ImageService;

/**
 * ImageLoadServiceImpl
 */
@Service
public class ImageServiceImpl implements ImageService {

  @Autowired
  private Environment env;


  @Override
  public void deleteImage(String filename) {
    try {
      Path file = ROOT.resolve(filename);
      Files.delete(file);
    } catch (Exception e) {
      throw new RuntimeException("Could not delete file! Error: " + e.getMessage());
    }
  }

  @Override
  public String saveImage(MultipartFile image, String path) {
    try {
      File file = new File(String.valueOf((ROOT)), path);
      if (!file.getParentFile().exists()) file.getParentFile().mkdir();
      boolean newFile = file.createNewFile();
      FileOutputStream output = new FileOutputStream(file);
      output.write(image.getBytes());
      output.close();
      final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
      String dir = "/images" + (path.startsWith("/")?"":"/");
      return baseUrl + dir + path;
    } catch (Exception e) {
      throw new RuntimeException("Could not store file! Error: " + e.getMessage());
    }
  }

  @Override
  public void init() {
    try {
      Files.createDirectory(ROOT);
    } catch (FileAlreadyExistsException e) {
      // do nothing
    } catch (IOException e) {
      throw new RuntimeException("Could not initialize folder for upload!");
    }
  }

  @Override
  public Resource loadImage(String filename) {
    try {
      Path file = ROOT.resolve(filename);
      Resource resource = new UrlResource(file.toUri());
      if (resource.exists() || resource.isReadable()) {
        return resource;
      } else {
        throw new RuntimeException("Could not read file: " + filename);
      }
    } catch (MalformedURLException e) {
      throw new RuntimeException("Could not load file! Error: " + e.getMessage());
    }
  }

  @Override
  public void deleteAll() {
    FileSystemUtils.deleteRecursively(ROOT.toFile());
  }

}