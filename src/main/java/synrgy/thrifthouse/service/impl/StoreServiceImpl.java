package synrgy.thrifthouse.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Data;
import synrgy.thrifthouse.dto.product.FavStoreDto;
import synrgy.thrifthouse.dto.product.StoreInformationDto;
import synrgy.thrifthouse.dto.store.*;
import synrgy.thrifthouse.dto.storeSettings.InformationDto;
import synrgy.thrifthouse.dto.storeSettings.InformationDto2;
import synrgy.thrifthouse.dto.storeSettings.StoreAddressDto;
import synrgy.thrifthouse.dto.storeSettings.StoreBankDto;
import synrgy.thrifthouse.dto.storeSettings.StoreDeliveryServiceDto;
import synrgy.thrifthouse.dto.user.SellerAccountDto;
import synrgy.thrifthouse.dto.user.SellerAccountDto2;
import synrgy.thrifthouse.exception.ConflictException;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.mapper.StoreMapper;
import synrgy.thrifthouse.model.FavoriteStore;
import synrgy.thrifthouse.model.FavoriteStoreKey;
import synrgy.thrifthouse.model.Store;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.FavoriteStoreRepository;
import synrgy.thrifthouse.repository.StoreRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.ImageService;
import synrgy.thrifthouse.service.StoreService;

@Service
public class StoreServiceImpl implements StoreService {

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StoreMapper storeMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ImageService imageService;

    @Autowired
    private FavoriteStoreRepository favoriteStoreRepository;

    @Override
    public StoreDetailBannerDto getBannerByStoreId(UUID id) {
        Optional<Store> storeOptional = this.storeRepository.findById(id);
        if (!storeOptional.isPresent()) {
            throw new ResourceNotFoundException("Store was not found");
        }

        return storeMapper.StoreToStoreDetailBannerDto(storeOptional.get());
    }

    @Override
    public Map<String, String> getLogoByStoreId(UUID id) {
        Optional<Store> storeOptional = this.storeRepository.findById(id);
        if (!storeOptional.isPresent()) {
            throw new ResourceNotFoundException("Store was not found");
        }
        Store store = storeOptional.get();
        Map<String, String> result = new HashMap<>();
        result.put("storeId", id.toString());
        result.put("url", store.getLogo());
        return result;
    }

    @Override
    public StoreInformationDto getStoreInformationById(UUID id) throws JsonMappingException, JsonProcessingException, IOException {
        Optional<Store> storeOptional = this.storeRepository.findById(id);
        if (!storeOptional.isPresent()) {
            throw new ResourceNotFoundException("Store was not found");
        }
        
        StoreInformationDto storeInformation = storeMapper.StoreToStoreInformationDto(storeOptional.get());
        ObjectMapper objectMapper = new ObjectMapper();

        Path name = Paths.get("src/main/resources/get-city-response.json");
        JsonNode cityResponse = objectMapper.readTree(
          new String(Files.readAllBytes(Paths.get(name.toUri()))).replaceAll("[\t\r\n]", "")
        );

        String cityId = "";
        String provinceId = "";
        boolean isFound = false;
        for (JsonNode result : cityResponse.path("rajaongkir").path("results")) {
          // store the city and province name
          String cityName = result.path("city_name").toString().split("\"")[1];
          String provinceName = result.path("province").toString().split("\"")[1];
          if (cityName.equals(storeInformation.getCity()) && provinceName.equals(storeInformation.getProvince())) {
            provinceId = result.path("province_id").toString().split("\"")[1];
            cityId = result.path("city_id").toString().split("\"")[1];
            isFound = true;
            break;
          }
        }
        if (!isFound) {
          storeInformation.setProvince_id("INVALID_NAME");
          storeInformation.setCity_id("INVALID_NAME");
        } else {
          storeInformation.setCity_id(cityId);
          storeInformation.setProvince_id(provinceId);
        }

        return storeInformation;
    }

    @Override
    public List<ProductsOfStoreDto> getStoreProducts(Integer page, Integer size, UUID id, String category) {
        Optional<Store> storeOptional = this.storeRepository.findById(id);
        if (!storeOptional.isPresent()) {
            throw new ResourceNotFoundException("Store was not found");
        }

        Pageable pageable = PageRequest.of(page, size);
        List<ProductsOfStoreDto> result = new ArrayList<>();
        if (category != null) {
            result = this.storeRepository.getProductsByStoreIdAndCategory(id, category, pageable);
        } else {
            result = this.storeRepository.getProductsByStoreId(id, pageable);
        }

        for (ProductsOfStoreDto dto : result) {
            String photosArray = dto.getPhotos();
            String firstPhoto = photosArray.substring(1, photosArray.length() - 1).split(", ")[0];
            dto.setPhotos(firstPhoto);
        }

        return result;
    }

    @Override
    public StoreAboutDto getStoreAbout(UUID id) {
        Optional<Store> storeOptional = this.storeRepository.findById(id);
        if (!storeOptional.isPresent()) {
            throw new ResourceNotFoundException("Store was not found");
        }

        return new StoreAboutDto(storeOptional.get().getAbout());
    }

    @Override
    public FavoriteStoreDto favoriteStore(UUID userId, FavoriteStoreDto favoriteStoreDto) {
        Optional<User> userOptional = userRepository.findById(userId);
        Optional<Store> storeOptional = storeRepository.findById(favoriteStoreDto.getStoreId());

        if (!userOptional.isPresent()) {
            throw new ResourceNotFoundException("Store with userId " + userId + " not found");
        }
        if (!storeOptional.isPresent()) {
            throw new ResourceNotFoundException("StoreId " + favoriteStoreDto.getStoreId() + " not found");
        }

        User user = userOptional.get();
        Store store = storeOptional.get();

        // Compare storeId in DB with storeId Params
        List<FavoriteStore> storeList = user.getStores()
                .stream().filter(dt -> dt.getId().getStoreId().compareTo(favoriteStoreDto.getStoreId()) == 0)
                .collect(Collectors.toList());

        if (storeList.size() == 0) {
            FavoriteStore favoriteStore = new FavoriteStore(
                new FavoriteStoreKey(userId, store.getId()),
                user,
                store
            );

            this.favoriteStoreRepository.save(favoriteStore);

            store.getFavorites().add(favoriteStore);
            Store saveFavorite = storeRepository.save(store);
            return storeMapper.storeToFavoriteStoreDto(saveFavorite);
        } else {
            user.getStores().removeAll(storeList);
            this.favoriteStoreRepository.deleteAll(storeList);
            userRepository.save(user);

            FavoriteStoreDto favoriteStoreDto1 = new FavoriteStoreDto();
            favoriteStoreDto1.setStoreId(favoriteStoreDto.getStoreId());
            return favoriteStoreDto1;
        }
    }

    @Override
    public PaginationStoreDto<List<FavStoreDto>> getAllFavoriteStore(UUID userId, String search, String urutkan, Integer page, Integer size) {
        final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User with userId " + userId + " was not found");
        }
        Pageable paging = PageRequest.of(page, size);

        Page<FavStoreDto> listFavoritePage = null;
        List<FavStoreDto> listFavorite = null;
        switch(urutkan) {
            case "terbaru_disimpan":
                listFavoritePage = storeRepository.getListFavoriteDesc(userId, search.toLowerCase(), paging);
                break;
            case "terakhir_disimpan":
                listFavoritePage = storeRepository.getListFavoriteAsc(userId, search.toLowerCase(), paging);
                break;
            case "abjad_awal":
                listFavoritePage = storeRepository.getListFavoriteAlphabeticalAsc(userId, search.toLowerCase(), paging);
                break;
            case "abjad_akhir":
                listFavoritePage = storeRepository.getListFavoriteAlphabeticalDesc(userId, search.toLowerCase(), paging);
                break;
          }

        listFavorite = listFavoritePage.stream().map(s -> {
            UUID storeId = s.getId();
            Store store = this.storeRepository.findById(storeId).get();
            FavStoreDto temp = storeMapper.StoreToFavStoreDto(store);
            s.setTotalProduct(temp.getTotalProduct());
            s.setTotalReview(temp.getTotalReview());
            s.setAverageReview(temp.getAverageReview());
            s.setFavoriteStore(temp.getFavoriteStore());
            return s;
        }).collect(Collectors.toList());

        return new PaginationStoreDto<>(listFavoritePage.getTotalElements(), listFavorite, listFavoritePage.getTotalPages(), listFavoritePage.getNumber());
    }

    @Override
    public ObjectNode createStore(UUID userId, StoreCreatePostDto storeCreatePostDto) {
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User was not found"));
        if (storeRepository.existsByUser_Id(userId)) {
            throw new ConflictException("Store Already Exist");
        }

        String extension = storeCreatePostDto.getKtpImg().getOriginalFilename();
        if (extension==null) throw new RuntimeException("Could not store files!");
        extension = extension.substring(extension.lastIndexOf("."));
        String now = LocalDateTime.now(ZoneId.of("Asia/Jakarta"))
                .format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HHmmss"));
        String filename = "ktpImg_" + user.getId() + "_" + now + extension;
        String s = imageService.saveImage(storeCreatePostDto.getKtpImg(), "/store/" + filename);

        Store store = storeMapper.StoreCreatePostDtoToStore(storeCreatePostDto);
        store.setUser(user);
        store.setKtpImg(s);
        Store save = storeRepository.save(store);

        StoreCreatePostDto newStore = storeMapper.StoreToStoreCreatePostDto(save);
        ObjectNode value = objectMapper.convertValue(newStore, ObjectNode.class);
        value.put("id", save.getId().toString());
        value.put("ktpImg", s);

        Path name = Paths.get("src/main/resources/get-city-response.json");
        JsonNode cityResponse;
        try {
            cityResponse = objectMapper.readTree(
                    new String(Files.readAllBytes(Paths.get(name.toUri()))).replaceAll("[\t\r\n]", "")
            );
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

        String cityId = "";
        String provinceId = "";
        boolean isFound = false;
        for (JsonNode result : cityResponse.path("rajaongkir").path("results")) {
            // store the city and province name
            String cityName = result.path("city_name").toString().split("\"")[1];
            String provinceName = result.path("province").toString().split("\"")[1];
            if (cityName.equals(save.getCity()) && provinceName.equals(save.getProvince())) {
                provinceId = result.path("province_id").toString().split("\"")[1];
                cityId = result.path("city_id").toString().split("\"")[1];
                isFound = true;
                break;
            }
        }
        if (!isFound) {
            value.put("province_id","INVALID_NAME");
            value.put("city_id","INVALID_NAME");
        } else {
            value.put("city_id",cityId);
            value.put("province_id",provinceId);
        }

        return value;
    }

    @Override
    public StoreDeliveryServiceDto getStoreDeliveryService(UUID id) {
        Optional<Store> storeOptional = this.storeRepository.findById(id);
        if (!(storeOptional.isPresent())) { throw new ResourceNotFoundException("Store was not found"); };
        Store store = storeOptional.get();
        return new StoreDeliveryServiceDto(store.getDeliveryService());
    }
    
    @Override
    public InformationDto2 getStoreInformation(UUID storeId) {
        Optional<Store> storeOptional = this.storeRepository.findById(storeId);
        if (!storeOptional.isPresent()) { throw new ResourceNotFoundException("Store was not found"); }
        Store store = storeOptional.get();
        return new InformationDto2(
            store.getName(),
            store.getBanner(),
            store.getLogo(),
            store.getStoreStatus(),
            store.getAbout()
        );
    }

    @Override
    public InformationDto2 updateStoreInformation(UUID storeId, InformationDto informationDto) {
        Optional<Store> storeOptional = this.storeRepository.findById(storeId);
        if (!storeOptional.isPresent()) { throw new ResourceNotFoundException("Store was not found"); }
        Store store = storeOptional.get();

        store.setName(informationDto.getStoreName() != null ? informationDto.getStoreName() : store.getName());
        store.setAbout(informationDto.getStoreAbout() != null ? informationDto.getStoreAbout() : store.getAbout());
        store.setStoreStatus(informationDto.getStoreStatus() != null ? informationDto.getStoreStatus() : store.getStoreStatus());
        
        // BANNER
        if (informationDto.getStoreBanner() != null) {
            if (!informationDto.getStoreBanner().isEmpty()) {
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
                formatter.setTimeZone(TimeZone.getTimeZone("GMT+7"));
                String dateNow = formatter.format(date);

                int getIndex = informationDto.getStoreBanner().getOriginalFilename().lastIndexOf('.');
                String extension = informationDto.getStoreBanner().getOriginalFilename().substring(getIndex);
                String formatPhoto   = "banner_"+ storeId +"_" + dateNow + extension;
                String newNamePhoto = imageService.saveImage(informationDto.getStoreBanner(), "/store-banner/" + formatPhoto);
                store.setBanner(newNamePhoto);
            }
        }

        // LOGO
        if (informationDto.getStoreLogo() != null) {
            if (!informationDto.getStoreLogo().isEmpty()) {
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
                formatter.setTimeZone(TimeZone.getTimeZone("GMT+7"));
                String dateNow = formatter.format(date);
                
                int getIndex = informationDto.getStoreLogo().getOriginalFilename().lastIndexOf('.');
                String extension = informationDto.getStoreLogo().getOriginalFilename().substring(getIndex);
                String formatPhoto   = "logo_"+ storeId +"_" + dateNow + extension;
                String newNamePhoto = imageService.saveImage(informationDto.getStoreLogo(), "/store-logo/" + formatPhoto);
                store.setLogo(newNamePhoto);
                store.setPhoto(newNamePhoto);
            }
        }

        store = this.storeRepository.save(store);
        return new InformationDto2(
            store.getName(),
            store.getBanner(),
            store.getLogo(),
            store.getStoreStatus(),
            store.getAbout()
        );
    }

    @Override
    public StoreBankDto getStoreBankAccount(UUID storeId) {
        Optional<Store> storeOptional = this.storeRepository.findById(storeId);
        if (!storeOptional.isPresent()) { throw new ResourceNotFoundException("Store was not found"); }
        Store store = storeOptional.get();
        return new StoreBankDto(store.getBankName(), store.getBankNumber(), store.getBankHolder());
    }

    @Override
    public StoreBankDto updateStoreBankAccount(UUID storeId, StoreBankDto storeBankDto) {
        Optional<Store> storeOptional = this.storeRepository.findById(storeId);
        if (!storeOptional.isPresent()) { throw new ResourceNotFoundException("Store was not found"); }
        Store store = storeOptional.get();
        store.setBankHolder(storeBankDto.getBankHolder() != null ? storeBankDto.getBankHolder() : store.getBankHolder());
        store.setBankNumber(storeBankDto.getBankNumber() != null ? storeBankDto.getBankNumber() : store.getBankNumber());
        store.setBankName(storeBankDto.getBankName() != null ? storeBankDto.getBankName() : store.getBankName());
        store = this.storeRepository.save(store);
        return new StoreBankDto(store.getBankName(), store.getBankNumber(), store.getBankHolder());
    }

    @Override
    public StoreAddressDto getStoreAddress(UUID storeId) {
        Optional<Store> storeOptional = this.storeRepository.findById(storeId);
        if (!storeOptional.isPresent()) { throw new ResourceNotFoundException("Store was not found"); }
        Store store = storeOptional.get();
        return new StoreAddressDto(store.getFullAddress(), store.getProvince(), store.getCity());
    }

    @Override
    public StoreAddressDto updateStoreAddress(UUID storeId, StoreAddressDto storeAddressDto) {
        Optional<Store> storeOptional = this.storeRepository.findById(storeId);
        if (!storeOptional.isPresent()) { throw new ResourceNotFoundException("Store was not found"); }
        Store store = storeOptional.get();
        store.setFullAddress(storeAddressDto.getFullAddress() != null ? storeAddressDto.getFullAddress() : store.getFullAddress());
        store.setProvince(storeAddressDto.getProvince() != null ? storeAddressDto.getProvince() : store.getProvince());
        store.setCity(storeAddressDto.getCity() != null ? storeAddressDto.getCity() : store.getCity());
        store = this.storeRepository.save(store);
        return new StoreAddressDto(store.getFullAddress(), store.getProvince(), store.getCity());
    }

    @Override
    public StoreDeliveryServiceDto updateStoreDeliveryService(UUID storeId, StoreDeliveryServiceDto deliveryServiceDto) {
        Optional<Store> storeOptional = this.storeRepository.findById(storeId);
        if (!storeOptional.isPresent()) { throw new ResourceNotFoundException("Store was not found"); }
        Store store = storeOptional.get();
        store.setDeliveryService(deliveryServiceDto.getDeliveryService() != null ? deliveryServiceDto.getDeliveryService() : store.getDeliveryService());
        store = this.storeRepository.save(store);
        return new StoreDeliveryServiceDto(store.getDeliveryService());
    }

    @Override
    public StoreKtpDto getStoreKtp(UUID store_id) {
        Store store = storeRepository.findById(store_id)
                .orElseThrow(() -> new ResourceNotFoundException("Store was not found"));
        return storeMapper.StoreToStoreKtpDto(store);
    }

    @Override
    public SellerAccountDto2 getSellerDetail(UUID userId) {
        SellerAccountDto2 response = new SellerAccountDto2();
        Optional<User> userOptional = this.userRepository.findById(userId);
        if (!userOptional.isPresent()) { throw new ResourceNotFoundException("User was not found"); }
        User user = userOptional.get();

        response.setFullname(user.getFullname());
        response.setEmail(user.getEmail());
        response.setPhone(user.getPhone());
        response.setBirth(user.getBirth() == null ? "Not set" : user.getBirth().toString());
        response.setGender(user.getGender() == null ? "Not set" : Character.toString(Character.toUpperCase(user.getGender())));
        
        Store store = user.getStore();
        if (store == null) { throw new ResourceNotFoundException("This seller doesn't have a store"); }
        response.setKtpImg(store.getKtpImg());
        response.setKtpNumber(store.getKtpNumber());

        response.setUser_id(userId);
        response.setStore_id(store.getId());

        return response;
    }

    @Override
    public SellerAccountDto2 updateSellerDetail(UUID userId, SellerAccountDto sellerAccountDto) {
        Optional<User> userOptional = this.userRepository.findById(userId);
        if (!userOptional.isPresent()) { throw new ResourceNotFoundException("User was not found"); }
        User user = userOptional.get();

        user.setFullname(sellerAccountDto.getFullname() != null ? sellerAccountDto.getFullname() : user.getFullname());
        user.setEmail(sellerAccountDto.getEmail() != null ? sellerAccountDto.getEmail() : user.getEmail());
        user.setPhone(sellerAccountDto.getPhone() != null ? sellerAccountDto.getPhone() : user.getPhone());

        if (user.getBirth() == null) {
            if (sellerAccountDto.getBirth() == null) {
                user.setBirth(new Date());
            } else if (sellerAccountDto.getBirth() != null) {
                user.setBirth(sellerAccountDto.getBirth());
            }
        } else {
            if (sellerAccountDto.getBirth() != null) {
                user.setBirth(sellerAccountDto.getBirth());
            }
        }
        
        user.setGender(sellerAccountDto.getGender() != null ? Character.toUpperCase(sellerAccountDto.getGender()) : user.getGender());
        
        Store store = user.getStore();
        if (store == null) { throw new ResourceNotFoundException("This seller doesn't have a store"); }
        store.setKtpNumber(sellerAccountDto.getKtpNumber() != null ? sellerAccountDto.getKtpNumber() : store.getKtpNumber());

        // KTP IMAGE
        if (sellerAccountDto.getKtpImg() != null) {
            if (!sellerAccountDto.getKtpImg().isEmpty()) {
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
                formatter.setTimeZone(TimeZone.getTimeZone("GMT+7"));
                String dateNow = formatter.format(date);

                int getIndex = sellerAccountDto.getKtpImg().getOriginalFilename().lastIndexOf('.');
                String extension = sellerAccountDto.getKtpImg().getOriginalFilename().substring(getIndex);
                String formatPhoto   = "sellerKtp_"+ userId +"_" + dateNow + extension;
                String newNamePhoto = imageService.saveImage(sellerAccountDto.getKtpImg(), "/seller-ktp/" + formatPhoto);
                store.setKtpImg(newNamePhoto);  
            }
        }

        user = this.userRepository.save(user);
        store = this.storeRepository.save(store);

        return new SellerAccountDto2(
            user.getId(),
            store.getId(),
            user.getFullname(),
            user.getEmail(),
            user.getPhone(),
            user.getBirth().toString(),
            Character.toString(user.getGender()),
            store.getKtpNumber(),
            store.getKtpImg().toString()
        );
    }

}

@Data
@AllArgsConstructor
class StoreAboutDto {

    private String about;

}
