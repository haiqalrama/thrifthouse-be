package synrgy.thrifthouse.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import synrgy.thrifthouse.dto.review.*;
import synrgy.thrifthouse.exception.BadRequestException;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.mapper.ReviewMapper;
import synrgy.thrifthouse.model.Review;
import synrgy.thrifthouse.model.ReviewKey;
import synrgy.thrifthouse.model.Store;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.ReviewRepository;
import synrgy.thrifthouse.repository.StoreRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.ImageService;
import synrgy.thrifthouse.service.ReviewService;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;
    private final ReviewMapper reviewMapper;
    private final UserRepository userRepository;
    private final StoreRepository storeRepository;
    private final ImageService imageService;

    public ReviewServiceImpl(ReviewRepository reviewRepository, ReviewMapper reviewMapper, UserRepository userRepository, StoreRepository storeRepository, ImageService imageService) {
        this.reviewRepository = reviewRepository;
        this.reviewMapper = reviewMapper;
        this.userRepository = userRepository;
        this.storeRepository = storeRepository;
        this.imageService = imageService;
    }

    @Override
    public ReviewListDto getReviews(Integer page, Integer size, Integer rate, Boolean hasPhoto, UUID storeId) {
        Optional<ReviewList> reviews = reviewRepository.findInformation(storeId);
        return reviews.map((review) -> {
            Pageable sortedByCreatedAt = PageRequest.of(page, size, Sort.by("createdAt").descending());

            Page<Review> all = null;
            if (rate!=null&&hasPhoto!=null) {
                if (hasPhoto) {
                    all = reviewRepository.findByStoreIdAndPhotosNotNull(sortedByCreatedAt,storeId,rate);
                } else {
                    all = reviewRepository.findByStoreIdAndPhotosNull(sortedByCreatedAt,storeId,rate);
                }
            } else if(rate!=null) {
                all = reviewRepository.findByStoreId(sortedByCreatedAt,storeId, rate);
            } else if (hasPhoto!=null) {
                if (hasPhoto) {
                    all = reviewRepository.findByStoreIdAndPhotosNotNull(sortedByCreatedAt,storeId);
                } else {
                    all = reviewRepository.findByStoreIdAndPhotosNull(sortedByCreatedAt,storeId);
                }
            } else {
                all = reviewRepository.findByStoreId(sortedByCreatedAt, storeId);
            }
            List<ReviewDto> sorted = all.map((review1 -> {
                ReviewDto reviewDto = reviewMapper.ReviewToReviewDto(review1);
                if (review1.isAnonim()) {
                    UserSimpleDto user = reviewDto.getUser();
                    user.setId(null);
                    user.setUsername("Anonymous");
                    reviewDto.setUser(user);
                }
                return reviewDto;
            })).stream().collect(Collectors.toList());
            ReviewListDto reviewListDto = reviewMapper.ReviewListToReviewListDto(review);
            reviewListDto.setReview(sorted);
            reviewListDto.setTotalItems(all.getTotalElements());
            reviewListDto.setTotalPages(all.getTotalPages());
            reviewListDto.setCurrentPage(all.getNumber());
            return reviewListDto;
        }).orElseThrow(() -> new ResourceNotFoundException("Store Id not found"));
    }

    @Override
    public ReviewDto postReview(UUID storeId, ReviewPostDto reviewPostDto, List<MultipartFile> photos) {
        if (reviewPostDto.getUserId() == null) throw new BadRequestException("User Id must be filled");
        String now = LocalDateTime.now(ZoneId.of("Asia/Jakarta"))
                .format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HHmmss"));
        String filename = "review_" + storeId + "_" + now + "_";
        List<String> filenames = null;
        if (photos!=null) {
            filenames = new ArrayList<>();
            for (int i = 0; i < photos.size(); i++) {
                String extension = photos.get(i).getOriginalFilename();
                if (extension == null) throw new RuntimeException("Could not store files!");
                extension = extension.substring(extension.lastIndexOf("."));
                String s = imageService.saveImage(photos.get(i), "/reviews/" + filename + i + extension);
                filenames.add(s);
            }
        }
        Review newReview = reviewMapper.ReviewPostDtoToReview(reviewPostDto);
        User user = userRepository.findById(reviewPostDto.getUserId())
                .orElseThrow(()-> new ResourceNotFoundException("User was not found"));
        Store store = storeRepository.findById(storeId)
                .orElseThrow(()-> new ResourceNotFoundException("Store was not found"));
        ReviewKey id = new ReviewKey(user.getId(),store.getId());
        newReview.setId(id);
        newReview.setStore(store);
        newReview.setUser(user);
        if (filenames != null) {
            newReview.setPhotos(filenames);
        }
        Review save = reviewRepository.save(newReview);
        ReviewDto reviewDto = reviewMapper.ReviewToReviewDto(save);
        if (save.isAnonim()) {
            UserSimpleDto dtoUser = reviewDto.getUser();
            dtoUser.setId(null);
            dtoUser.setUsername("Anonymous");
            reviewDto.setUser(dtoUser);
        }
        return reviewDto;
    }
}
