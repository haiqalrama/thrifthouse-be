package synrgy.thrifthouse.service;

import java.util.List;
import java.util.UUID;

import synrgy.thrifthouse.dto.user.UserDto;

public interface UserService {
    
    List<UserDto> getUsers();
    UserDto getUserById(UUID id);
    void insertUser(UserDto userDto);
    void updateUser(UserDto userDto, UUID id);
    void deleteUser(UUID id);
    UUID findStoreOwnerByStoreId(UUID storeId);
    UUID findStoreByUserId(UUID userId);

}
