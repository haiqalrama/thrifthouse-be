package synrgy.thrifthouse.service;

import java.util.UUID;

import synrgy.thrifthouse.dto.dashboard.DashboardSummaryDto;
import synrgy.thrifthouse.dto.dashboard.TransactionDetailDto;

public interface DashboardService {
    
    DashboardSummaryDto getDashboardSummary(UUID userId, Integer page, Integer size, String status, String sortOrder, String id_transaksi);
    TransactionDetailDto getTransactionDetail(UUID orderId);
    void deleteTransaction(UUID orderId);
    void generateTransactionsForOneStore(UUID userId);

}
