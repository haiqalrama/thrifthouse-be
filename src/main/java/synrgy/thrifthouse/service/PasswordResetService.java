package synrgy.thrifthouse.service;

public interface PasswordResetService {
    void createPasswordReset(String email);
    void resetPassword(String token, String password);
}
