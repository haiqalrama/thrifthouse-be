package synrgy.thrifthouse.service;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;

import synrgy.thrifthouse.dto.product.ProductWithStoreDto;

public interface CartService {
  public void addToCart(UUID userId, UUID productId);

  public void removeFromCart(UUID userId, UUID productId);

  public Collection<ProductWithStoreDto> getCart(UUID userId) throws IOException;

  void bulkAddToCart(UUID userId, Set<UUID> productIds);

  void bulkRemoveFromCart(UUID userId, Set<UUID> productIds);
}