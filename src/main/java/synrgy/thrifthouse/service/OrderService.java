package synrgy.thrifthouse.service;

import java.util.Map;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;
import synrgy.thrifthouse.dto.order.OrdersPaymentDto;
import synrgy.thrifthouse.dto.order.UpdateOrderDto;
import synrgy.thrifthouse.dto.order.InsertOrderDto;
import synrgy.thrifthouse.dto.order.InsertOrderDtoNew;

public interface OrderService {
    
    Object insertOrder(InsertOrderDto insertOrderDto);
    Map<String, String> insertOrderNew(InsertOrderDtoNew insertOrderDto);
    OrdersPaymentDto getOrdersPayment(UUID id);
    void updateOrderStatus(UUID id, UpdateOrderDto updateOrderDto, String status);
    String storePaymentReceipt(MultipartFile image, UUID id);
    void updateTransactionStatus(UUID transactionId, String orderStatus, String packagingStatus);
    void generateTransactionsForStores();
    
}
