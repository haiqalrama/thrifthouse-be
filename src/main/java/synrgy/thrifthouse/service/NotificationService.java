package synrgy.thrifthouse.service;

import synrgy.thrifthouse.dto.notification.NotificationDto;

import java.util.List;
import java.util.UUID;

public interface NotificationService {
    List<NotificationDto> getAllNotificationByUserId(UUID userId, String type);

    NotificationDto markAsRead(UUID userId, UUID notificationId);

    NotificationDto createNotification(UUID userId, NotificationDto notificationDto);
}
