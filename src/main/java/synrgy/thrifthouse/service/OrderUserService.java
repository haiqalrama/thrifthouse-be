package synrgy.thrifthouse.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface OrderUserService {

    List<Map<String, Object>> getAll(UUID userId, String status);
    void acceptOrder(UUID orderId);

    Map<String, Object> getOrderDetailById(UUID userId, UUID orderId);
}
