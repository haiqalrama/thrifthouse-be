package synrgy.thrifthouse.service;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {

  public final Path ROOT = Paths.get("src/main/resources/uploads");

  public void init();

  public Resource loadImage(String filename);

  public void deleteAll();

  public void deleteImage(String filename);

  public String saveImage(MultipartFile image, String path);
}
